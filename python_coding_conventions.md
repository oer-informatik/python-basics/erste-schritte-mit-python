# Coding Conventions


## Automatisch auf PEP8-Konformität überprüfen
Benötigte Pakete Installieren
```shell
python -m pip install pycodestyle
```

## Konformitätstest ausführen
```shell
pycodestyle --first .\test_multiply_kata.py
```
# Konventionen

## Variablennamen

## Funktionsnamen


## pep8

## Automatisiertes Testen von pep8-Konformität
