## Objektsammlungen / Übersicht

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_objektsammlungen</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Python bietet vier wesentliche Strukturen für Objektsammlugnen: List, Tupel, Dictionary, Set. In diesem Artikel werden die Unterschiede kurz vorgestellt._


### Tabellarische Übersicht

Zur groben Einordnung der verfügbaren Objektsammlungen soll hier nur die Notation der entsprechenden Literale dargestellt werden. Die wichtigsten Objektsammlungen sind:

|Datentyp|Beschreibung|Zuweisung|
|-------|------|------|
|`list`|Veränderliche Liste|`liste=[1,2,3]`|
|`tupel`|Unveränderliche Liste|`menge=(1,2,3)`|
|`dict`|Assoziative Liste|`kartenwert={"Bube":1,"Dame":2, "König":3, "Ass":11, "10":10}`|
|`set`|Sammlung einzigartiger Elemente|`karten={"Herz", "Karo", "Kreuz", "Pik"}`|


### Liste

Die Elemente von Listen können geändert, gelöscht und angefügt werden (sie sind mutable). Wie bei Tupeln kann auf die einzelnen Elemente über deren Index zugegriffen werden und es können Dopplungen enthalten sein. Listen sind in ihrer Länge variabel und verfügen über eine innere Ordnung.

```python
>>> gewaehlt=["Stein", "Schere", "Stein", "Papier", "Schere"]
>>> type(gewaehlt)
<class 'list'>
>>> gewaehlt
['Stein', 'Schere', 'Stein', 'Papier', 'Schere']
>>> gewaehlt[2]
'Stein'
```

Details zum Umgang mit Listen finden sich [in diesem Artikel](https://oer-informatik.de/python_listen)


### Tupel

Bei Tupeln handelt es sich um Listen, deren Elemente unveränderlich (_immutable_) sind. Sie werden in Runde Klammern eingefasst. Das selbe Objekt kann mehrfach im Tupel vorhanden sein. Auf die einzelnen Elemente kann über einen Index (beginnend bei 0) zugegriffen werden.

```python
>>> symbole=("Schere", "Stein", "Papier")
>>> type(symbole)
<class 'tuple'>
>>> symbole
('Schere', 'Stein', 'Papier')
>>> symbole[0]
'Schere'
```

### Dictionary

Dictionaries sind assoziative Listen aus Key-Value-Paaren, bei denen der Zugriff auf Einzelelemente (Value) nicht über einen Zahlenindex erfolgt, sondern über beliebige eindeutige Schlüssel (Key).

```python
>>> kartenwert={"Bube":1,"Dame":2, "König":3, "Ass":11, "10":10}
>>> type(kartenwert)
<class 'dict'>
>>> kartenwert
{'Bube': 1, 'Dame': 2, 'König': 3, 'Ass': 11, '10': 10}
>>> kartenwert['Ass']
11
```

Ein Artikel zu den Grundlagen der [Dictionary findet sich hier](https://oer-informatik.de/python_dictionary).

### Set

Sets sind ungeordnete Sammlungen einzigartiger (unikaler) Elemete (also ohne Dopplungen).

```python
>>> karten={"Herz", "Karo", "Kreuz", "Pik"}
>>> type(karten)
<class 'set'>
>>> karten
{'Pik', 'Kreuz', 'Herz', 'Karo'}
```

Details zum Umgang mit Sets finden sich [in diesem Artikel](https://oer-informatik.de/python_set)