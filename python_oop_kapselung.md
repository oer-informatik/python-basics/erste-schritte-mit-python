## Das Konzept der Kapselung in Python

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109880041862304525</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_oop_kapselung</span>

> **tl/dr;** _(ca. 15 min Lesezeit): Wer gut änderbaren Code programmieren will, der versteckt Implementierungsdetails wie den Zustand von Objekten. Veröffentlicht wird nur, was wirklich benötigt wird. In Python ist es nicht ganz einfach (und eher unüblich), den Rest wegzukapseln. Aber wie lässt sich Kapselung in Python umsetzen?_

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Grundidee der Kapselung

Eines der Grundprinzipien der Objektorientierung ist die Kapselung: Auf den Zustand eines Objekts sollte nur über Methoden des Objekts, nicht jedoch direkt über die Attribute zugegriffen werden. Es werden für lesende Operationen Getter-Methoden, für schreibende Operationen Setter-Methoden implementiert. In einem ersten Entwurf können diese folgendermaßen aussehen (auf die spezielle Python-Implementierung gehen wir weiter unten ein).

```python
class Kunde:

    def __init__(self):
        self.name: str

    def get_name(self) -> str:
        return self.name

    def set_name(self, name: str):
        self.name = name

mein_kunde = Kunde()
mein_kunde.set_name("Max Mustermann")
```

Im UML-Klassendiagramm stellt sich diese Klasse folgendermaßen dar:

![UML-Klassendiagramm für die Kunde mit dem Attribut `name` sowie Getter- und Setter-Methoden](plantuml/kunde-klasse_basic.png)


### Vorteile der Kapselung

Welche Vorteile ergeben sich aus dem Implementierungsaufwand für Getter- und Settermethoden?

* die konkrete Implementierung und Datenstruktur kann geändert werden, ohne dass die bisherigen Objektaufrufe angepasst werden müssen. Beispielsweise kann in der Klasse `Kunde` oben der `name` in `vorname` und `nachname` aufgeteilt werden. Es müssen nur die Getter- und Settermethoden angepasst werden, damit die Klasse 100% kompatibel zu bisherigen Aufrufen bleibt:

```python
class Kunde:

    def __init__(self):
        self.vorname: str
        self.nachname: str

    def get_name(self) -> str:
        return self.vorname + " " + self.nachname

    def set_name(self, name: str):
        self.vorname, self.nachname = name.split(" ", 1)
```

* es können (auch im Nachgang) Validierungen und Plausibilisierungen vorgenommen werden. Beispielsweise kann eine Eingabe, bevor sie als Attributwert gespeichert wird von _Whitespaces_ am Rand befreit werden oder der Umgang mit `None`-Werten bei Ausgabe explizit geregelt werden:

```python
    def set_vorname(self, vorname: str):
        self.vorname = vorname.strip() # entfernt Whitespaces am Start/Ende

    def get_vorname(self) -> str:
        return "unknown" if self.vorname is None else self.vorname
```

* Attribute, die sich aus anderen Attributen berechnen / ergeben können auf eine gemeinsame Datenbasis zugreifen. Wenn später die Datenbasis geändert werden soll, kann dies ohne Probleme getan werden:

```python
class Rechnung:

    def __init__(self):
        self.rechnungssumme_netto: float = 0
        self.mwst_proz: float = 19.0

    def set_rechnungssumme_netto(self, rechnungssumme_netto:float):
        self.rechnungssumme_netto = rechnungssumme_netto

    def get_rechnungssumme_netto(self)-> float:
        return self.rechnungssumme_netto

    def get_rechnungssumme_brutto(self)-> float:
        return self.rechnungssumme_netto*(100+self.mwst_proz)/100
```

### Erste Umsetzung der Kapselung in Python: wir verstecken so gut es geht

Die Klasse aus obigem Beispiel sieht zunächst - ohne versteckte Attribute - so aus:

```python
class Kunde:

    def __init__(self):
        self.name: str

    def get_name(self) -> str:
        return self.name

    def set_name(self, name: str):
        self.name = name


mein_kunde = Kunde()
mein_kunde.set_name("Max Mustermann")
```

Im Gegensatz zu anderen Programmiersprachen bietet Python keine Möglichkeit, den Zugriff auf Instanzvariablen für Aufrufe außerhalb des Objekts zu sperren. Alle Instanzvariablen sind zunächst öffentlich (_public_).

Um den Zugriff darauf zu erschweren, gibt es eine Reihe von Konventionen:

- Instanzvariablen, die mit einem _Underscore_ beginnen, gelten als privat (sollen also nur intern, aus dem Objekt selbst gelesen und verändert werden). Der Zugriff von außen ist weiterhin möglich, aber die Namenskonvention verrät: es ist nicht mehr gewünscht. Beides zeigt das folgende Beispiel:

```python
class Kunde:

    def __init__(self, name: str) -> None:
        self.set_name(name)

    def get_name(self) -> str:
        return self._name

    def set_name(self, name: str):
        self._name = name

mein_kunde = Kunde("Hans Wurst")
print(mein_kunde.get_name())           
mein_kunde._name = "Paulchen Panther" # Zugriff direkt möglich
print(mein_kunde._name)               # Zugriff direkt möglich
```

- Instanzvariablen, die mit zwei _Underscore_ beginnen, können von außen nur über einen abgeänderten Namen, dem ein Unterstrich und der Klassenname vorangestellt sein muss, erreicht werden. Auch hier ist der Zugriff weiterhin möglich, jedoch mit angepasstem Namen:

```python
class Kunde:

    def __init__(self, name: str) -> None:
        self.__name: str
        self.set_name(name)

    def get_name(self) -> str:
        return self.__name

    def set_name(self, name: str):
        self.__name = name

mein_kunde = Kunde("Hans Wurst")
print(mein_kunde.get_name())           
mein_kunde._Kunde__name = "Paulchen Panther" # Zugriff über den veränderten
                                             # Attributnamen ist direkt möglich
print(mein_kunde.get_name())
print(mein_kunde._name)         # Zugriff über den unveränderten
                                # Attributnamen nicht möglich
```

Beide Fälle sind jedoch nur mehr oder weniger gut "versteckt" und nicht wirklich _private_. Im UML-Klassendiagramm kennzeichnet man _private_ Attribute mit dem Sichtbarkeitsmodifizierer "Minus", _public_-Methoden mit "Plus":

![UML-Klassendiagramm für die Kunde mit dem Attribut `name` sowie Getter- und Setter-, diesmal mit Sichtbarkeitsmodifizierern](plantuml/kunde-klasse_gekapselt.png)

### Der Königsweg der Kapselung in Python: Properties

Um den Zugriff über eine zentrale Schnittstelle zu erhalten werden in Python _properties_ genutzt. Hierzu werden neben dem Attribut optional auch die Getter- und Settermethoden über doppelte Unterstriche versteckt. Das von außen sichtbare Attribut wird als sogenannte _property_ angelegt. Einer Property werden die jeweiligen Getter- und Settermethodennamen als Parameter übergeben. So weiß Python, welche Methode im Fall eines lesenden und eines schreibenden Zugriffs genutzt werden muss.

```python
class Kunde:

    def __init__(self, name: str) -> None:
        self.__name: str
        self.__set_name(name)

    def __get_name(self) -> str:
        print("Lese den Namen "+self.__name+" aus")
        return self.__name

    def __set_name(self, name: str):
        print("Ändere den Namen von "+self.__name+" zu "+ name)
        self.__name = name

    def __del_name(self):
        print("Lösche den Namen")
        self.__name = ""
        # del self.__name würde das ganze Attribut löschen

    name = property(fget=__get_name, fset=__set_name, fdel=__del_name, doc="Name eines Kunden")


mein_kunde = Kunde("Hans Wurst")          
mein_kunde._Kunde__set_name("Johnny Rotten")
mein_kunde.name="Sid Vicious"
del mein_kunde.name
print(mein_kunde.name)
```

In obigen Beispiel wurde zusätzlich noch ein "Deleter" und ein Docstring für die _property_ übergeben.

Es gibt in Python noch einen eleganteren Weg, _properties_ zu implementieren: wir **dekorieren** die Getter- und Setter-Methoden. Das ist eine Syntaxerweiterung von Python, um Code übersichtlicher zu machen. Wichtig hierbei ist, dass wir zuerst die Gettermethode implementieren, mit `@property` dekorieren (es vor die Methode schreiben) und mit dem Namen der Property versehen:

```python
class Kunde:

    def __init__(self, name: str) -> None:
        self.__name : str
        self.__name = name

    @property
    def name(self) -> str:
        print("Lese den Namen "+self.__name+" aus")
        return self.__name

    @name.setter
    def name(self, name: str):
        print("Ändere den Namen von "+self.__name+" zu "+ name)
        self.__name = name


mein_kunde = Kunde("Hans Wurst")          
mein_kunde.name="Sid Vicious"
print(mein_kunde.name)
print(type(mein_kunde.name))
```

Was passiert im Hintergrund? Ein _Decorator_ ist ein verschachtelter Funktionsaufruf. Immer wenn `name` aufgerufen wird, wird statt `name` zunächst `property` aufgerufen, was dann wiederum `name` aufruft. Das wird vielleicht etwas deutlicher, wenn man die synonyme Schreibweise hierfür darstellt.

```python
@property
def name(self) -> str:
    pass
```

ist gleichbedeutend mit

```python
def name(self) -> str:
    pass
name = property(name)
```

Moment - das ist ja genau die Schreibweise von oben ohne _Decorator_ und mit einem Parameter (und angepasstem Methodennamen)? Decorator sind also kein Zauberwerk, sondern nur _syntactic sugar_: eine Vereinfachung in der Schreibweise.

Wer sich noch nicht lange mit Python beschäftigt hat mag etwa überrascht sein, dass man auch Funktionen (oder hier: Methoden) als Parameter übergeben kann. Es lohnt sich, sich mit dem Gedanken anzufreunden, da es ein sehr mächtiges Werkzeug ist.

Folgt man dem Gedanken, so können wir auch die synonyme Schreibweise für den Setter formulieren:

```python
@name.setter
def name(self, name: str):
    pass
```

ist gleichbedeutend mit (Funktionsnamen angepasst, da dies sonst zu Problemen führt):

```python
def set_name(self, name: str):
  pass
name = name.setter(set_name)
```

`name` ist ja zu diesem Zeitpunkt bereits eine _property_ und verfügt demnach über eigene Methoden (getter, setter, deleter). Diese Methoden dekorieren jetzt die selbst implementierte Setter-Methode (umklammern diese also und rufen sie auf). Auch bei Protperty handelt es sich nur um eine Klasse, deren Objekte Zustand (Referenzen auf die Funktionen) und Verhalten (getter/setter-Funktionen) haben.

![Ein Beispiel mit der Klasse Kunde und der Klasse Property](plantuml/kunde-klasse_gekapselt_property.png)



### Weitere Literatur zu `object`

- [Python-Dokumentation](https://docs.python.org/3/library/)

