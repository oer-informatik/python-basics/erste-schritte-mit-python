## Python/UML Übungsaufgabe: eine erstes Pacman-Inkrement

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_pygame_oop-pacman</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110315713318697657</span>

> **tl/dr;** _(ca. 90 min Bearbeitungszeit): Im Rahmen der Übungsaufgaben soll ein erstes Inkrement eines vereinfachten Pacman-Spiels mit Python objektorientiert entworfen und implementiert werden. Ausgeblendete Beispielantworten mit Links sind gegeben._

### Entwurf eines stark vereinfachten _Pacman_-Spiels

Es soll ein vereinfachtes Pacman-Spiel entworfen und entwickelt werden. Die Anforderungen sind in folgenden Stichpunkten umrissen:

* Das Spielfeld ist ein Rechteck. Das bei Pacman übliche Labyrinth wird zunächst nicht umgesetzt. Auf dem Spielfeld sind in regelmäßigem Abstand Punkte verteilt. Diese Punkte müssen durch den Spieler ("Pacman") gefressen werden.

![Stark vereinfachtes Pacman-Spielfeld aus Punkten und Puc](images/pacman/pacman_basic.png)

* Pacman selbst wird als einfacher Kreis dargestellt. Er kann sich per Tastatureingabe nach oben, unten, rechts und links bewegen. 

* Auf allen Koordinaten, auf denen Pacman war, sind die Punkte "aufgegessen" und werden nicht mehr angezeigt.

* Ziel des Spiels in der ersten Iteration ist es, möglichst schnell alle Punkte zu essen.

Der Quelltext wurde bereits vorbereitet und findet sich [hier(link)](src/pygame_pacman_basic.py). Dieser Quelltext ist Grundlage der folgenden Aufgaben. 

### UML-Klassendiagramm entwerfen

Ergänze anhand des oben verlinkten Quelltextes die Klasse "Pacman" mit allen dort genannten Details in dem folgenden UML-Diagramm. Eine Beziehung zwischen "Pacman" und "Background" muss nicht eingezeichnet werden.

![UML-Diagramm der Klasse Background](plantuml/pacman-background.png)

 <button onclick="toggleAnswer('uml_all')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="uml_all">

![UML-Diagramm der Klassen Background und Pacman](plantuml/pacman-mit-background.png)

Die Beziehung zwischen Background und Pacman muss nicht dargestellt werden, sondern nur die Klasse Pacman.

_Zum Nachlesen finden sich Infos zum UML-Klassendiagramm [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm)._

</span>

### Fragen zur Objektorientierung

a) Das Spiel wurde mit den zwei Klassen (`Pacman` und `Background`) realisiert. Wie bezeichnet man die Objektbeziehung, die zwischen der Instanz von `Pacman` und der Instanz von `Background` existiert? Begründen Sie ihre Wahl anhand des Quelltextes! <button onclick="toggleAnswer('objektbeziehung')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="objektbeziehung">

`Background` und `Pacman` stehen in einer Teil-Ganzes-Beziehung. Im Quelltext kann man das daran erkennen, dass `Background` ein Attribut vom Typ `Pacman` hält:

```python
class Background:
    def setup(self):
        self.__pacman = Pacman(step = self.__dot_size)  
```

Es kann sich also um eine Aggregation oder um eine Komposition handeln. Um eine Komposition handelt es sich, wenn das Löschen des `Background` auch den verknüpften `Pacman` mit löscht. Das ist insoweit gegeben, als im Programm keine Referenz dieses `Pacman` außerhalb von `Background` existiert (also z.B. auch die Objekterzeugung von `Pacman` innerhalb von `Background` stattfindet) und das Attribut - im Rahmen der Möglichkeiten von Python - als `private` gekennzeichnet ist. Die Beziehung ist also eine Komposition.

_Zum Nachlesen finden sich Infos zu Objektbeziehungen [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-assoziation)._

</span>



b) Wofür steht das "`+`" in dem obigen UML-Klassendiagramm bei der Methode `setup()`? (Name der Eigenschaft und Erklärung) <button onclick="toggleAnswer('sichtbarkeita')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="sichtbarkeita">

Das "`+`" an Attributen und Methoden im Klassendiagramm steht für den Sichtbakeitsmodifikator `public`. Der betreffende Member ist von allen anderen Programmteilen sichtbar und aufrufbar.

_Zum Nachlesen finden sich Infos zu Sichtbarkeitsmodifikatoren [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)._

</span>

c) Wofür steht das "`-`" in dem obigen UML-Klassendiagramm bei dem Attribut `dot_size`? (Name der Eigenschaft und Erklärung) <button onclick="toggleAnswer('sichtbarkeitb')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="sichtbarkeitb">

Das "`-`" an Attributen und Methoden im Klassendiagramm steht für den Sichtbakeitsmodifikator `private`. Der betreffende Member soll nur innerhalb des eigenen Objekts sichtbar und aufrufbar sein.

_Zum Nachlesen finden sich Infos zu Sichtbarkeitsmodifikatoren [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)._

</span>


d) Worin unterscheiden sich Assoziationen von Aggregationen und Kompositionen in der Objektorientierten Programmierung (OOP)? Grenzen Sie die Begriffe voneinander ab und nennen Sie die Notation im UML-Diagramm!

Assoziation: <button onclick="toggleAnswer('ass')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="ass">

Eine Assoziation stellt eine allgemeine Objektbeziehung dar. Zwei Objekte arbeiten temporär zusammen - etwa dadurch, dass ein Objekt Methoden oder Attribute des anderen Objekts nutzt. Im Gegensatz zu Aggregation und Komposition stellen Assoziationen keine Teil-Ganzes-Beziehung ("A besteht aus B") dar, sondern können allgemein mit der Bezeichnung "A nutzt B" beschrieben werden.

Assoziationen werden durch einfache Linien zwischen Objekten dargestellt, an deren Ende mit Hilfe von Pfeil oder Kreuz die Navigierbarkeit dargestellt werden kann. 

Wie bei Aggregationen und Kompositionen können Mulitplizitäten an den Enden und Namen mit Leserichtung an der Linie angegeben werden.

_Zum Nachlesen finden sich Infos zu Objektbeziehungen [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-assoziation)._

</span>

Aggregation: <button onclick="toggleAnswer('agg')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="agg">

Eine Aggregation ist eine Teil-Ganzes-Beziehung, bei der ein Teil auch unabhängig vom Ganzen existieren kann. Dies wird realisiert, in dem ein Teil als Attribut in einem Ganzen organisiert wird. Teile können in diesem Fall auch mehreren Ganzen zugeordnet sein (und Ganze mehrere Teile aggregieren).

_Zum Nachlesen finden sich Infos zu Objektbeziehungen [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-assoziation)._

</span>

Komposition: <button onclick="toggleAnswer('kom')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="kom">

Eine Komposition ist eine starke Teil-Ganzes-Beziehung, bei der ein Teil existenzabhängig von einem Ganzen ist: wird ein Ganzes gelöscht, so werden auch dessen Teile gelöscht. Dies wird realisiert, in dem ein Teil als Attribut in einem Ganzen organisiert wird und keine Referenzen auf das verknüpfte Teil außerhalb des Ganzen existieren. Ein Ganzes kann mehrere Teile besitzen, ein Teil aber zu höchstens einem Teil gehören.

_Zum Nachlesen finden sich Infos zu Objektbeziehungen [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-assoziation)._

</span>

e) Wie nennt man die Methode `__init__()`, was ist ihre Aufgabe und was sind ihre Eigenschaften? <button onclick="toggleAnswer('init')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="init">

Bei `__init__()` handelt es sich um den Konstruktor, der bei jeder Objekterzeugung aufgerufen wird und die Argumente übernimmt, die bei der Objekterzeugung übergeben wurden (Beispiel: `pacman = Pacman(color=(10,255,0))`). In der Regel werden im Konstruktor die Attribute mit übergebenen Parameterwerten oder Defaultwerten gesetzt und alle Operationen ausgeführt, die nötig sind, damit das Objekt erstellt wird.

Bei der Methode `__init__()` handelt es sich um eine _magic method_, also um eine Methode, die nicht direkt aufgerufen werden soll, daher ist sie mit Doppelunterstrichen markiert.

_Zum Nachlesen finden sich Infos Konstruktoren in der OOP allgemein [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm)._

</span>

f) Was versteht man in der Objektorientierung unter "Kapselung" und was sind die Vorteile dieses Prinzips?<button onclick="toggleAnswer('kapselung')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="kapselung">

Bei Kapselung handelt es sich um eine Technik, mit deren Hilfe der Zustand eines Objektes vor dem Zugriff von außen versteckt wird. Der Zugriff kann dann nicht mehr direkt über die Attribute erfolgen, sondern über Getter- und Settermethoden.

Ein Vorteil der Kapselung ist, dass der Code weniger Abhängigkeiten enthält und leichter zu ändern ist. Da sichergestellt ist, dass niemand direkt auf das Attribut zugreift, kann die Art und Weise wie dieser Zustand gespeichert wird später geändert werden. Lediglich die Getter und Setter müssen weiter existieren. Ein weiterer Vorteil ist, dass an zentraler Stelle Validierung und Anpassungen erfolgen können, bevor ein Zustand ein- und ausgegeben wird.

_Zum Nachlesen finden sich Infos zu Sichtbarkeitsmodifikatoren und Kapselung in der UML allgemein [in diesem Artikel](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren). Ein spezieller Artikel zur Möglichkeit in Python zu kapseln [findet sich hier](https://oer-informatik.de/python_oop_kapselung)._

</span>

g) Warum ist Kapselung in Python nicht leicht umzusetzen? <button onclick="toggleAnswer('pythonkapsel')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="pythonkapsel">

Python verfügt über keine Sichtbarkeitsmodifizierer im klassischen OOP-Stil. Hier können Attribute und Methoden lediglich versteckt (_dunder_ - mit doppelten Unterstrichen) oder als versteckt markiert werden (mit einem Unterstrich). Faktisch ist der Zugriff auf die Attribute so aber immer noch möglich.

Anstelle von Getter- und Settermethoden (z.B. `get_name()` und `set_name(name:str)`) werden daher bei Python oft Properties genutzt, wenn gekapselt werden soll.

Noch weiter verbreitet ist jedoch in Python, auf Kapselung komplett zu verzichten.

_Zum Nachlesen finden sich Infos zur Möglichkeit in Python zu kapseln [hier](https://oer-informatik.de/python_oop_kapselung)._

</span>

h) Aus welchen Gründen haben sich die Erfinder von Python dagegen entschieden, Sichtbarkeitsmodifizierer in ihre Sprache zu integrieren? <button onclick="toggleAnswer('a1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a1">

Der Philosophie von Python nach sind Entwickler*innen verantwortlich für den gesamten Code und sollen damit auch verantwortlich umgehen können - dazu gehört, dass davon ausgegangen wird, dass man sich nicht selbst aus Codebereichen aussperren muss.

</span>



### Implementierung einer Methode

Es soll eine weitere Klasse erstellt werden: in Pacman gibt es Geister, die sich auf den Pacman zubewegen.

![UML-Diagramm der Klasse Background](plantuml/pacman-ghost.png)

Implementiere anhand der Klasse Ghost (siehe UML-Diagramm oben) die Methode `move()`, die als Parameter den Pacman übergeben bekommt. In der Methode `move()` soll sich der Geist eine halbe Einheit nach oben bewegen, wenn Pacman sich oberhalb des Geistes befindet, nach unten, wenn er unterhalb ist, nach rechts, wenn er rechts des Geistes ist und nach links, wenn er sich links befindet. Die Bewegung wird durch Anpassung der Koordinaten `x_position` und `y_position` umgesetzt. <button onclick="toggleAnswer('a2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a2">

```python
    def move(self, pacman):
        if (self.__y_position > pacman.y_position):
            self.__y_position += -1
        if (self.__y_position < pacman.y_position):
            self.__y_position += +1
        if (self.__x_position > pacman.x_position):
            self.__x_position += -1
        if (self.__x_position < pacman.x_position):
            self.__x_position += +1
```

</span>

### Implementierung einer Superklasse

a) Pacman und die Geister haben eine Menge Gemeinsamkeiten. Entwerfen Sie ein UML-Klassendiagramm, in dem die  gemeinsamen Superklasse `Figure` mit ihren Attributen und Methoden steht sowie die beiden Klassen `Pacman` und `Ghost` (bei beiden müssen keine Methoden/Attribute genannt werden). Ergänzen Sie die Beziehungen zwischen den Klassen. <button onclick="toggleAnswer('umlinherit')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="umlinherit">

![Die Klasse Figure](plantuml/pacman-vererbung.png)

(Zur Beantwortung der Frage langt die Klasse Figure sowie die Klassen `Pacman` und `Ghost`, diese jedoch ohne Attribut.)

_Zum Nachlesen finden sich Infos zur Vererbung in UML und Python [hier](https://oer-informatik.de/uml-klassendiagramm-vererbung)._

</span>

b) Wie müssen die Attribute der Superklasse im UML-Klassendiagramm angelegt werden, damit `Pacman` und `Ghost` direkt auf diese zugreifen können?  <button onclick="toggleAnswer('attributinherit')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="attributinherit">

Die Sichtbarkeit der Attribute darf nicht privat sein oder die Attribute müssen Getter und Setter (oder Properties) zur Verfügung stellen.

(In anderen Programmiersprachen wäre der Königsweg, das Attribut als _protected_ zu deklarieren, damit es innerhalb der Vererbungshierarchie zugreifbar ist. Diese Möglichkeit gibt es aber in Python nicht.)

_Zum Nachlesen finden sich Infos zur Vererbung in UML und Python [hier](https://oer-informatik.de/uml-klassendiagramm-vererbung)._

</span>

c) Erstellen Sie den Klassenrumpf und den Konstruktor von `Ghost` in Python. <button onclick="toggleAnswer('klassenrumpf')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="klassenrumpf">

```python
class Ghost(Figure):
    def __init__():
        super().__init__()
```

</span>

### Pygame Framework

Erstellen Sie für das Hauptprogramm (die Game-Loop) einen Programm-Ablaufplan (PAP). Es sollen nur die Aufrufe des Hauptprogramms dargestellt werden – Vorgänge im inneren der Objekte müssen nicht dargestellt werden. <button onclick="toggleAnswer('a3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a3">

![PAP der Gameengine des Programms](images/PacmanGameEnginePAP.png)

</span>

### Listen mit Python

Es sollen Listenoperationen vorgenommen werden. Als Hilfe dient das folgende UML-Klassendiagramm

![Die Klasse `list` im Klassendiagramm](plantuml/list.png)

a) Erstellen Sie mit einem Python Befehl eine neue leere Liste namens `ghostnames`: <button onclick="toggleAnswer('a4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a4">

```python
ghostnames = []
```

</span>



b) Weisen Sie der Liste in einem Befehl die Geisternamen "Blinky", "Stinki", "Ponky" und "Inky" zu.<button onclick="toggleAnswer('a5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a5">

```python
ghostnames = ["Blanky", "Stinki", "Ponky", "Inky"]
```

</span>


c) Fügen Sie mit einem Python Befehl "Clyde" hinten an die Liste an.<button onclick="toggleAnswer('a6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a6">

```python
ghostnames.append("Clyde")
```

</span>



d) Löschen Sie mit einem Python Befehl das Element mit dem Index 1! <button onclick="toggleAnswer('a7')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a7">

```python
ghostnames.pop(1)
```

</span>



e) Fügen Sie mit einem Python Befehl an die 2. Stelle "Pinky" an! <button onclick="toggleAnswer('a8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a8">

```python
ghostnames.insert(1, "Pinky")
```

</span>



f) Löschen Sie das Element mit dem Inhalt "Ponky" aus der Liste! <button onclick="toggleAnswer('arem8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="arem8">

```python
ghostnames.remove("Ponky")
```

</span>

f) Ändern Sie den Namen des ersten Elements ("Blanky") zu "Blinky"! <button onclick="toggleAnswer('aup8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="aup8">

```python
ghostnames[0] = "Blinky"
```

</span>

g) Geben Sie mit einem Python Befehl den Inhalt der Liste aus!
<button onclick="toggleAnswer('a9')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a9">

```python
print(ghostnames)
```

</span>



g) Welchen Inhalt hat die Liste jetzt?<button onclick="toggleAnswer('a10')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="a10">

```python
["Blinky", "Pinky", "Inky", "Clyde"]
```

</span>



### Links und weiter Informationen

- Sehr lesenswert hierzu sind die Beiträge in Jörg Kantels [Blog Schockwellenreiter](https://kantel.github.io/posts/2023010701_pygame_oop/), der in der Objektorientierung noch einen Schritt weiter geht, und auch die Oberfläche selbst zum Objekt macht (und andere interessante Artikel hat)

- Die Website vom Framework [PyGame](https://www.pygame.org/wiki/GettingStarted)
