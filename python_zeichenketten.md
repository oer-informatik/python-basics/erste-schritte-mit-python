## Zeichenketten in Python

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_zeichenketten</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Eine Kurzübersicht der wesentlichen Methoden, die Python für Zeichenketten bietet._

Texte werden in Zeichenketten (Strings) erfasst. Die Literale von Zeichenketten können entweder zwischen einfachen oder doppelten Anführungszeichen notiert werden.

### Beispiel

```python
>>> name = "Martina Mustermann"
>>> type(name)
<class 'str'>
>>> name
'Martina Mustermann'
```

### Besondere Zeichen

Um Sonderzeichen als Literal darstellen zu können müssen diese maskiert werden:

|Sonderzeichen|Maskierung|Beispiel|
|-------|------|:------|
|Zeilenumbruch| `\n` |`print("Hallo\nWelt")`|
|Tabulator| `\t` |`print("Hallo\tWelt")`|
|Backslash| `\\` |`print('HomeDir H:\\\\')`|
|Anführungszeichen| `\"`,  `\'`,|`print("Hallo \"Welt\"")` <br/> `print('Hallo "Welt"')` <br/> `print('Hallo \'Welt\'')`|

### Operatoren

|Operator|Name|Beispiel |
|-------|------|:------|
|`text + text`| Konkatenierung|`"Hallo"+'Welt'`|
|`text*zahl`| Vervielfachung|`"Ha "*3`|
|`teilstring in text`| Prüft, ob `teilstring` enthalten ist|`"Martina" in "Martina Mustermann"`|
|`text[i]`]|Zugriff auf den i-ten Buchstaben (beginnend bei 0)|`"abc"[2]`|

### Funktionen und Methoden für Zeichenketten

|Operator|Name|Beispiel |
|-------|------|:------|
|`len(text)`| Länge des Strings `text`|`len("Martina Mustermann")`|
|`str(variable)`| Wandelt `variable`in String um|`str(4+5)`|
|`text.lower()`<br>`text.upper()`| Wandlet den String `text` in Klein- bzw. Großbuchstaben um|`"GmbH".lower()`|
|`text.startswith()`<br>`text.endswith()`| Prüft Anfang und Ende des Strings|`"Windows".startswith("Win")`|
|`text.count(teilstring)`| Zählt die Vorkommen von `teilstring`|`"Bananenmarmelade".count("an")`|
|`text.find(teilstring [, startpos])`| Gibt die Position des ersten Vorkommens von `teilstring` zurück (ggf. ab `startpos`) |`"Bananenmarmelade".find("ma")`|
|`text.replace(suchtext, ersetztext)`| Ersetzt `suchtext` durch `ersetztext` |`"Hihihi".replace("i","a")`|

Falls Du genauere Informationen benötigst, hilft die Python-Hilfe in der Pythonshell, z.B.:

```python
>>> help(str.find)
Help on method_descriptor:

find(...)
    S.find(sub[, start[, end]]) -> int

    Return the lowest index in S where substring sub is found,
    such that sub is contained within S[start:end].  Optional
    arguments start and end are interpreted as in slice notation.

    Return -1 on failure.
```


