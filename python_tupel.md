## Objektsammlungen / Tupel

# Tupel

Bei Tupeln handelt es sich um Listen, deren Elemente unveränderlich (_immutable_) sind. Sie werden in Runde Klammern eingefasst. Dasselbe Objekt kann mehrfach im Tupel vorhanden sein. Auf die einzelnen Elemente kann über einen Index (beginnend bei 0) zugegriffen werden.

```python
>>> symbole=("Schere", "Stein", "Papier")
>>> type(symbole)
<class 'tuple'>
>>> symbole
('Schere', 'Stein', 'Papier')
>>> symbole[0]
'Schere'
```
## Weitere Literatur zu Listen

- [Python-Dokumentation zu Listen](https://docs.python.org/3/library/stdtypes.html#sequence-types-list-tuple-range)
