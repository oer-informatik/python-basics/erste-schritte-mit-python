## Datentypen in Python (v.a.: Zahlentypen)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_datentypen</span>

> **tl/dr;** _(ca. 6 min Lesezeit):  Zahlen, Zeichenketten, Wahrheitswerte: welche Datentypen bietet Python an? Der Schwerpunkt hier liegt auf den numerischen Typen für Gleitkomma- und Ganzzahlen: ist `0b1` eine gültige Zahl und `1e1`? Und: warum in aller Welt ist das hier `False`: `0.1 + 0.2 == 0.3`?!?_

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

Python bietet eine Reihe von internen (_build-in_) Datentypen an, aus deren Kombination alle weiteren Datentypen erstellt werden können. Eine vollständige Liste findet sich in der [Python Dokumentation](https://docs.python.org/3/library/stdtypes.html). Für den Anfang genügen die Datentypen für Zahlen, Zeichen und Wahrheitswerte sowie Objektsammlungen.

Die wichtigsten einfachen Datentypen sind:

|Datentyp|Beschreibung|Zuweisung|
|-------|------|:------|
|`int`| Ganzzahlen|`anzahl = 12`|
|`float`|Gleitkommazahlen|`temperatur = 36.5`|
|`bool`|Wahrheitswerte|`ist_wahr = True`|
|`str`|Zeichenkette|`name='Hannes'`|

### Zahlentypen

Im Gegensatz zu vielen anderen Programmiersprachen kennt Python von Haus aus nur zwei Zahlentypen: `int` für Ganzzahlen und `float` für Gleitkommazahlen. Die Wertebereiche beider Zahlen werden dynamisch angeglichen. Mit Hilfe der _PythonShell_ lassen sich die Zahlentypen leicht erkunden:

#### Ganzzahlen

```python
>>> jahr = 2020
>>> type(jahr)
<class 'int'>
>>> jahr
2020
```

Abhängig von der Anzahl der genutzten Bit können unterschiedliche viele Zahlen abgespeichert werden. Mit einem Bit können maximal 2<sup>1</sup>  = 2 Zahlen gespeichert werden, bei 8 Bit 2<sup>8</sup> = 256. Sofern positive und negative Zahlen abgebildet werden, ist ein Bit nötig, um das Vorzeichen abzubilden. Da die $0$  als positive Zahl gewertet wird, ist dieser Bereich um eine Zahl kleiner. Allgemein gesprochen ist die höchste abbildbare Ganzzahl bei einem vorzeichenbehafteten Datentypen mit $n$ Bit:

$$2^{n-1}-1$$

Bei vielen Programmiersprachen ist ein `int` auf 32 Bit Datenbreite begrenzt, die höchste abbildbare Zahl wäre also 2<sup>32-1</sup>-1 (also 2.147.483.647), darüber hinaus gibt es häufig einen `long`-Datentyp mit 64 Bit (bis maximal 2<sup>64-1</sup>-1, das sind 9.223.372.036.854.775.807).

In Python jedoch ist die Datenbreite von `int` dynamisch: Es lassen sich auch extrem große Zahlen darstellen, beispielsweise 2 hoch 100.000: eine Zahl mit ca. 10.000 Stellen:

```python
>>> 2**100000
```

(die PythonShell fragt dann gewöhnlich nach, ob man wirklich die gesamte Ausgabe sehen will...)

### Ganzzahlen in anderen Zahlensystemen

Wir stellen Ganzzahlen üblicherweise im mit dezimalen Literalen dar ( Stellenwertsystem mit Ziffern [0-9]). Literale nennt man in diesem Zusammenhang die Darstellungsform eines Wertes im Programmtext. In der Informatik sind häufig weitere Zahlensysteme relevant, die wir mit Python auch direkt als ganzzahlige Literale dieser Stellenwertsysteme darstellen können:

* Dualzahlen (Binärsystem):

  Das Stellenwertsystem baut auf der Basis ``2`` auf, und verfügt über die Ziffern (Zeichenvorrat) 0 und 1 ([0-1]). Die Literale im Binärsystem werden mit vorangestelltem `0b` notiert. Die Python-Shell übersetzt direkt ins Dezimalsystem:

  ```python
  >>> 0b10
  2
  >>> 0b110
  6
  ```

* Oktalzahlen [0-7]:

  Das Stellenwertsystem baut auf der Basis ``8`` auf, und verfügt über die Ziffern (Zeichenvorrat) 0 bis 7 ([0-7]).

  ```python
  >>> 0o10
  8
  >>> 0o110
  72
  ```

* Hexadezimalsystem (16er-System):

  Das Stellenwertsystem baut auf der Basis ``16`` auf, und verfügt über die Ziffern (Zeichenvorrat) 0 bis 9 und A bis F ([0-9A-F]).

  ```python
  >>> 0x10
  8
  >>> 0x110
  272
  ```

### Gleitkommazahlen

Gleitkommazahlen werden als Literal mit dem Dezimalpunkt eingegeben (Bsp.: `1.1`). In der Regel werden Zahlenwerte als Gleitkommazahl umgesetzt, wenn an den Operationen eine Gleitkommazahl beteiligt ist oder wenn eine Division (Bsp.: `10/4`) vorgenommen wird.

```python
>>> usbstick_preis = 9.99
>>> type(usbstick_preis)
<class 'float'>
>>> usbstick_preis
9.99
```

Sehr kleine und sehr große Zahlen werden in der Exponentialschreibweise angegeben - wie etwa auch bei Taschenrechnern:

```python
>>> elementarladung = 1.602176634e-19
>>> avogadro_konstante= 6.02214076e23
```
(Das entspricht 1.602176634\*10<sup>-19</sup> bzw. 6.02214076\*10<sup>23</sup>)

#### Interne Speicherung von Gleitkommazahlen

Gleitkommazahlen werden in Python mit 64-Bit Datenbreite gespeichert. Intern werden sie als binäre Exponentialzahl gespeichert, nach dem Schema (vereinfacht):

Gleitkommazahl = Vorzeichen * Mantisse * 2<sup>(Exponent)</sup>

Wobei der höchste Exponenten +1023 ist (11 Bit, Vorzeichenbehaftet) und die höchste Mantisse "fast" eins (0.5 + 0.25 + 0.125 +0.625... 52 Bit lang). Daraus ergibt sich die höchste Zahl zu annähernd 2<sup>(1023)</sup>.

Wir lassen uns von Python helfen, um dies Zahl in eine dezimale Exponentialzahl umzuwandeln. Der Exponent auf Basis der Zahl 10 wäre:

```python
>>> import math
>>> math.log(2**1023,10)
307.95368556425274
```

Die höchste Gleitkommazahl, die mit 64 Bit gespeichert werden kann, liegt also fast bei 2*10<sup>308</sup> - eine Zahl mit 308 Stellen!

#### Randprobleme bei Gleitkommazahlen

Aus der internen Abbildung von Gleitkommazahlen folgen eine Reihe von Einschränkungen:

- Viele einfache Dezimalzahlen lassen sich binär nicht präzise darstellen. Die Zahl 0.1 ist ein einfaches Beispiel. Daraus folgen ggf. unerwartete Probleme in der Programmierung:

  ```python
  >>> 0.2 + 0.1 == 0.3
  False
  ```

- viele Zahlen teilen sich die selbe Binärdarstellung. Beispielsweise sind theoretisch alle Zahlen 0, deren Mantisse 0 ist. Aber auch sonst können sich unverständliche Resultate einstellen:

  ```python
  >>> 0.1 == 0.10000000000000001
  True
  ```

### Fazit

Wenn wir Zahlen in Python speichern wollen, unterscheiden wir also in der Regel zwischen Gleitkommazahlen und Ganzzahlen. Ganzzahlen können nahezu beliebig skaliert werden (es gibt in Python im Gegensatz zu anderen Programmiersprachen keine festgelegten Obergrenzen). Bei Gleitkommazahlen dürfen wir nie auf Gleichheit überprüfen, weil die interne Umrechnung ins Binärsystem einige einfache Dezimal-Gleitkommazahlen nicht präzise darstellen kann (einfaches Beispiel: `0.1`).

### Nächste Schritte

Mit einem sicheren Verständnis für Datentypen können wir uns jetzt auf das nächste Thema stürzen: [Operatoren](https://oer-informatik.de/python_operatoren).

### Links und weitere Informationen

* [Python Dokumentation zu Datentypen](https://docs.python.org/3/library/stdtypes.html)
