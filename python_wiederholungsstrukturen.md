## Wiederholungsstrukturen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_wiederholungsstrukturen</span>

> **tl/dr;** _(ca. 12 min Lesezeit): Wer die Wahl hat, hat die Qual: mit den alternativen Kontrollstrukturen (If-Statements) halten die Suites (Einrückungen) erstmals Einzug in Python. Und die Enttäuschung, weil Mehrfachauswahlen in Python recht umständlich realisiert werden müssen._


Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Zählergesteuerte Schleifen

In Python werden _zählergesteuerte Schleifen_ über die Methode `range()` realisiert. Soll beispielsweise eine Operation zehnmal ausgeführt werden, so wird sie folgendermaßen notiert:

```python
for i in range(10):
    print(i)
```

In der Variablen `i` wird jeweils die Anzahl der Durchläufe gespeichert. Augenscheinlich werden zwar 10 Werte augegeben, jedoch von 0 bis 9.  (Python ist also Null-Index-basiert, wie die meisten anderen Programmiersprachen). Die `range()`- Methode lässt jedoch auch zu, dass wir bei jedem erdenklichen Wert starten und stoppen. Der Startwert wird immer im ersten Durchlauf genutzt, der Stopwert ist derjenige Wert, bei dem die Schleife nicht mehr ausgeführt wird. Folgendes Beispiel würde also nur neun Werte (1-9) ausgeben:


```python
start = 1
stop = 10

for i in range(start, stop):
    print("aktuell: ", i)
```

Start- und Stoppwert können auch direkt in die `range()`-Funktion eingegeben werden:

```python
for i in range(5, 7):
    print("aktuell: ", i)

print("Am Ende: ", i)
```

Die Zählvariable (hier: `i`) bleibt auch nach der Schleife erhalten und kann ausgelesen werden.
Neben dem Start- und Stoppwert kann auch noch die Schrittweite jedes Schleifendurchlaufs angegeben werden, per _default_ ist das `1`:

```python
zaehle_von = 1
zaehle_bis_vor = 10
schrittweite = 1
for i in range(zaehle_von, zaehle_bis_vor, schrittweite):
    print("aktuell: ", i)
```

Die Schrittweite kann jedoch auch andere Ganzzahlen als Wert haben - auch negative Zahlen sind möglich, dann wird rückwärts gezählt.

```python
for i in range(10, 6, -2):
    print("aktuell: ", i)

print("Am Ende: ", i)
```

#### `break` und `continue`

Bei `continue` wird nur der aktuelle Schleifendurchlauf abgebrochen und mit dem nächsten Schleifendurchlauf begonnen.

```python
for i in range(1, 10, 1):
   if i%2==0:
       print(i, " gerade")
       continue
   else:
       print(i, " ungerade")
   print("-------")
```

Nachdem ausgegeben wurde, dass es sich um eine gerade Zahl handelt wird direkt in den nächsten Schleifendurchlauf gesprungen. Die Ausgabe von `-------` erfolgt daher nur nach ungeraden Zahlen:

```
1  ungerade
-------
2  gerade
3  ungerade
-------
4  gerade
5  ungerade
-------
6  gerade
7  ungerade
-------
8  gerade
9  ungerade
-------
```

Weitgehender ist der Abbruch bei `break`: hier wird die gesamte Schleife abgebrochen:

```python
for i in range(1, 10, 1):
    if i%2==0:
        print(i, " gerade")
        break
    else:
        print(i, " ungerade")
    print("-------")
```

Sobald die erste gerade Zahl auftritt, wird die komplette Schleife abgebrochen:

```
1  ungerade
-------
2  gerade
```

#### Python-Besonderheit: der `else`-Block bei Schleifen

Im Gegensatz zu den meisten anderen Programmiersprachen bietet Python die Möglichkeit, einen `else`-Block an die Schleife anzuhängen, der ausgeführt wird, wenn die Schleife beendet wurde. Nur in dem Fall, dass das Schleifenende durch ein `break`-Statement hervorgerufen wurde, wird diese _Suite_ nicht ausgeführt.

```python
for i in range(1, 2):
    if i%2==0:
        print(i, " gerade")
        break
    else:
        print(i, " ungerade")
    print("-------")
else:
    print("Regulär beendet")
```    

### Kopfgesteuerte Wiederholungsstrukturen

```python
eingabe = ""
while eingabe != "exit":
    eingabe = input('Beenden mit "exit": ')

print("Tschüss")
```

```
Beenden mit "exit": Guten Morgen!
Beenden mit "exit": Hallo
Beenden mit "exit": exit
Tschüss
```

Analog zu zählergesteuerten Schleifen funktionieren hier auch `break`, `continue` und `else`:

```python
eingabe = ""
while eingabe != "exit":
    eingabe = input('Beenden mit "exit": ')
    if eingabe == "ende":
        break
else:
    print("Bis bald")
print("Tschüss")
```

### Fußgesteuerte Wiederholungsstrukturen

In vielen Programmiersprachen wird unterschieden zwischen Schleifen, deren Bedingung vor dem ersten Durchlauf geprüft wird (kopfgesteuert / `while`) und Schleifen, die einmal durchlaufen werden, und erst vor der ersten Wiederholung die Bedingung prüfen (fußgesteuert / `do while`). Python kennt diese Unterscheidung nicht.

Wir haben aber bei dem obigen Beispiel bereits eine Schleife programmiert, deren Operationen in jedem Fall einmal durchlaufen werden. Die Schleifenbedingung haben wir vor Eintritt in die Schleife mit `eingabe = ""` auf `True` gesetzt, und so einen ersten Durchlauf garantiert. Auf diese Weise muss man fußgesteuerte Schleifen in Python implementieren.

```python
eingabe = ""
while eingabe != "exit":
    eingabe = input('Beenden mit "exit": ')

print("Tschüss")
```

Wer den Unterschied zwischen kopfgesteuerten und fußgesteuerten Schleifen nochmal bildhaft unterstützt lernen will, der sollte sich das Bild von den Zeichentrickfiguren _Coyote_ und _Roadrunner_ mal ansehen, z.B. [hier](https://bytefreaks.net/funny/wile-e-coyote-and-roadrunner-demonstrating-programming-bugs)


### Iteration durch Objektsammlungen mit Wiederholungsstrukturen

Schleifeniterationen sind nicht nur durch `range()`-Objekte möglich, sondern auch durch andere Objektsammlungen. Der Aufbau ist immer ähnlich:

```python
for var in elemente:
  anweisungen
```

Beispielsweise werden Zeichenketten als Sammlung einzelner Zeichen begriffen. Dieser Aufruf buchstabiert demnach übergebene Wörter:

```python
for i in "Hallo":
    print("aktuell: ", i)
```

### Fazit

Auch bei Wiederholungsstrukturen weist Python einige Besonderheiten im Vergleich zu anderen Programmiersprachen auf:

* Es werden häufiger `for`-Schleifen gewählt. Mit dem `range`-Objekt verfügt Python über eine Syntax, die stark von der anderer Sprachen abweicht.

* `do-while`-Schleifen gibt es gar nicht, sie müssen selbst aus einer While-Schleife erstellt werden, in dem die Schleifenbedingung vorab auf `True` gesetzt wird.

* Ein `else`-Block bei Schleifen mag auf den ersten Blick verwundern, es gibt aber in Kombination mit dem `break`-Block durchaus sinnvolle Anwendungsfälle.

## Nächste Schritte

Mit Wiederholungsstrukturen und bedingten Anweisungen werden die Programme zunehmend komplex. Höchste Zeit, sie in einzeln aufrufbare Codeblöcke zu unterteilen: [die Funktionen](https://oer-informatik.de/python_funktionen).

## Links und weitere Informationen

- [Der Unterschied zwischen `do-while` und `while-do` als Comic](https://bytefreaks.net/funny/wile-e-coyote-and-roadrunner-demonstrating-programming-bugs)


