## Operatoren in Python

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_operatoren</span>

> **tl/dr;** _(ca. 6 min Lesezeit):  Es gibt eine ganze Reihe Operatoren für Zahlen in Python: die aritmetischen Operatoren, Vergleichsoperatoren, Zuweisungsoperatoren, bitweise Operatoren... Und dann gibt es noch die Ausführungsreihenfolge. Was in aller Welt ergibt denn `zahl = 2**4-4%3>0b100<<2`?!?_

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Operatoren bei Zahlentypen

Python bietet für Zahlentypen die üblichen arithmetischen Operatoren. Es werden die gleichen Operatoren verwendet, wie in den meisten Programmiersprachen (vielleicht mit Ausnahme der Exponentialfunktion `**`)`:`

|Operator|Name|Beispiel |
|-------|------|------|
|a+b| Addition|`10+5`|
|a-b| Subtraktion|`10-5`|
|a\*b| Multiplikation|`10*5`|
|a/b| Division (Ergebnis immer `float`)|`10/5`|
|a//b| Ganzzahlige Division ("Ergebnis abgerundet")|`10//6`|
|a%b| Modulo (Rest bei a//b)|`10%6`|
|a\*\*b| Exponentialfunktion ("hoch")|`2**10`|

Bei den arithmethischen Operationen können in der Regel die beiden Zahlentypen `int` (Ganzzahl) und `float` (Gleitkommazahl) kombiniert werden. Hierbei gilt die Regel: Sollte an der Operation ein `float` beteiligt sein oder eine Division vorkommen, so ist das Ergebnis ein `float`. Sind alle Operanden `int`, so ist auch das Ergebnis `int` (Ausnahme: Division `/`):

```python
>>> type(25/5)
<class 'float'>
>>> type(25*5)
<class 'int'>
>>> type(25*5.0)
<class 'float'>
>>> type(25//5)
<class 'int'>
>>> type(25.0//5)
<class 'float'>
```

Per `int()` und `float()` lassen sich die Zahlen-Datentypen ineinander umwandeln (_casten_).

```python
>>> int(5.0)
5
>>> type(int(5.0))
<class 'int'>
```

```python
>>> float(5)
5.0
>>> type(float(5))
<class 'float'>
```

### Wahrheitswert

Wahrheitswerte speichern die binäre Zustände: wahr oder falsch (`0` oder `1`, Elektrotechnik: `HIGH` oder `LOW`). Sie werden in Variablen des Datentyps `bool` gespeichert und mit den Literalen `True` und `False` notiert. In der Regel werden sie über Vergleichsoperatoren gebildet.

#### Beispiel

```python
>>> stimmt_nicht = 3<4
>>> type(stimmt_nicht)
<class 'bool'>
>>> stimmt_nicht
True
```

#### Vergleichsoperatoren zur Bestimmung von Wahrheitswerten

Wahrheitswerte werden häufig mit Hilfe von Vergleichen erzeugt. Hierzu können die folgenden Vergleichsoperatoren genutzt werden.

|Operator|Name|Beispiel |
|-------|------|------|
|`>`, `>=`| Zahlenwerte größer (... gleich)|`10.0>5`|
|`<`, `<=`| Zahlenwerte kleiner (... gleich)|`7 <= 5`|
|`==`, `!=`| Wertgleichheit, Wertunterschiedlichkeit|`7==5`|
|`is`, `is not`| selbes Objekt, unterschiedliches Objekt|`datei1 is datei2`|
|`in`, `not in`| Element in Objektsammlung oder String vorhanden| `5 in (1,2,3,4)`|

#### Logische Operatoren zur Verknüpfung von Wahrheitswerten

Wahrheitswerte lassen sich miteinander kombinieren. Hierbei kann die Ausführungsreihenfolge über Klammern bestimmt werden. Werden keine Klammern gesetz, so wird zuerst `not`, dann `and`, dann `or` ausgeführt.

|Operator|Name|Beispiel |
|-------|------|------|
|`and`| logisches UND|`4>3 and 7>5`|
|`or`| logisches ODER|`1>3 or 7>5`|
|`not`| logisches NICHT|` not 1>3`|

#### Logische Operatoren und `None`

Besonderes Augenmerk sollte bei logischen Vergleichen darauf gerichtet sein, wenn eine der Variabeln keinen Wert aufweist, sie also ein `None`-Objekt ist. Hierbei gelten die folgenden Regeln:

|a |a and `None`| a or `None`|
|------|------|------|
| `True`|**None**|**True**|
| `False`|**False**|**None**|
| `None`|**None**|**None**|

`not None` entspricht `True`

#### Bitweise Operatoren

In Python können nicht nur die Arithmetischen Operatoren für Dezimalzahlen genutzt werden, wir können auch direkt Binärzahlenoperationen vornehmen. Die einfachsten Operationen sind hier die Verschiebung der Bitfolge nach rechts oder links. Beim Verschieben nach rechts geht dabei die Stelle ganz rechts verloren.

|Operator|Name|Beispiel |
|-------|------|------|
|`>>`| Bit-Shiften um angegebene Stellen nach rechts|`bin(0b00001000>>1) == '0b00000100'`<br/>`bin(0b00001000>>2) == '0b00000010'`|
|`<<`| Bit-Shiften um angegebene Stellen nach links|`bin(0b00001000<<1) == '0b00010000'`<br/>`bin(0b00001000<<2) == '0b00100000'`|
|`&`| bitweises UND |`bin(0b11110000 & 0b01010101) == '0b1010000'`|
|`|`| bitweises ODER |`bin(0b11110000 | 0b01010101) == '0b11110101'`|

Das Bitweise UND und ODER verleicht jede einzelne Stelle der Binärzahl mit der jeweiligen Stelle der zweiten Binärzahl. Diese Operation wird beispielsweise in Netzwerken beim Subnetting durchgeführt, um die Netzadresse zu erhalten.


### Zuweisungs-Operatoren

In Python können nicht nur die Arithmetischen Operatoren für Dezimalzahlen genutzt werden, wir können auch direkt Binärzahlenoperationen vornehmen. Die einfachsten Operationen sind hier die Verschiebung der Bitfolge nach rechts oder links. Beim Verschieben nach rechts geht dabei die Stelle ganz rechts verloren.

|Operator|Name|Beispiel |
|-------|------|------|
|`=`|Zuweisung|`zahl = 1`|
|`+=`|Zuweisungsinkrement <br/>Zahl benötig vorher Wertzuweisung<br/>Vorheriger Wert wird um den Wert auf der rechten Seite erhöht.|`zahl = 1`<br/>`zahl += 2` <br/> _zahl: 3_|
|`-=`|Zuweisungsdekrement <br/>Zahl benötig vorher Wertzuweisung<br/>Vorheriger Wert wird um den Wert auf der rechten Seite vermindert.|`zahl = 5`<br/>`zahl -= 2`<br/> _zahl: 3_|
|`*=`|Zuweisungsprodukt <br/>Zahl benötig vorher Wertzuweisung<br/>Vorheriger Wert wird mit dem Wert auf der rechten Seite multipliziert.|`zahl = 2`<br/>`zahl *= 3`<br/> _zahl: 6_|
|`/=`|Zuweisungsdivision <br/>Zahl benötig vorher Wertzuweisung<br/>Vorheriger Wert wird durch den Wert auf der rechten Seite geteilt.|`zahl = 24`<br/>`zahl /= 6`<br/> _zahl: 4.0_|
|`**=`|Zuweisungspotenzierung <br/>Zahl benötig vorher Wertzuweisung<br/>Vorheriger Wert wird mit dem Wert auf der rechten Seite potenziert.|`zahl = 9`<br/>`zahl **= 2`<br/> _zahl: 81_|
|`//=`|Ganzzahlige Zuweisungsdivision <br/>Wie Zuweisungsdivision, nur ganzzahlig.|`zahl = 30`<br/>`zahl //= 7`<br/> _zahl: 4_|
|`%=`|Zuweisungsmodulo <br/>Wie Zuweisungsdivision, nur ganzzahliger Rest wird zugewiesen.|`zahl = 30`<br/>`zahl %= 7`<br/> _zahl: 2_|


### Reihenfolge in der Operatorauswertung

Werden nicht Auswertungsreihenfolgen durch Klammerung festgelegt, so wertet Python in der folgenden Reihenfolge die Operatoren aus:

1. Exponentialfunktion (`**`)
2. Vorzeichen (`+`/`-`)
3. "Punkt vor Strich"
3a.  Multiplikation/Division (`*`, `/`, `//`)
3b. Addition und Subtraktion (`+`, `-`)
4. binäre Operatoren (BitShift `>>`, `<<`, binäres und/oder `&`, `|`)
5. Vergleichsoperatoren (`>`, `<`,  `>=`, `<`, `is`, `in`, `==`, `!=`)
6. logische Operatoren (erst `not`, dann `and`, zuletzt `or`)
7. Zuweisungsoperatoren (`=`, `+=`, `-=`, `*=`, `/=`, `**=`, `//=`, `%=`)

### Fazit

Neben den arithmetischen Operatoren und Vergleichs- und Zuweisungsoperatoren gibt es noch eine ganze Reihe mehr oder weniger skurriler weiterer Möglichkeiten. Im Gegensatz zu vielen Programmiersprachen fehlen die Inkrement-Operatoren (`i++`, `++i`), wir müssen hier ein Zeichen mehr investieren: `i+=1`. Wichtig bei alledem: wenn wir uns bei der Ausführungsreihenfolge nicht ganz sicher sind, dann lohnt es, clever Klammern zu setzen. Ich jedenfalls müsste erstmal die obige Liste suchen, um vorhersagen zu können, welchen Wert `zahl` hier hat:

`zahl = 2**4-4%3>0b100<<2`

(Spoiler: `True`)

Mit einem sicheren Verständnis für Datentypen und Operatoren können wir uns jetzt auf das nächste Thema stürzen: [alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen).

### Nächste Schritte

Als nächstes wenden wir uns dem Kontrollfluss zu: [Alternativen / Verzweigungen mit `if`](https://oer-informatik.de/python_alternative_kontrollstrukturen).

