## Objektorientierung in Python

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109880041862304525/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_oop</span>


> **tl/dr;** _(ca. 10 min Lesezeit): In der realen Welt trifft man häufig auf Dinge, die ein identisches Verhalten aber unterschiedliche Eigenschaften haben. Objektorientierte Programmierung hilft dabei, Einheiten zu modellieren, die über ein gemeinsames Verhalten bei unterschiedlichen Zuständen verfügen._

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Was kennzeichnet Objekte, was kennzeichnet Klassen?

Wenn wir Objekte in der realen Welt beschreiben, nennen wir oft zwei unterschiedliche Aspekte: die Eigenschaften (den Zustand) sowie das Verhalten dieser Objekte. Auch abstrakte, nicht greifbare Dinge wie ein Bankkonto lassen sich so beschreiben:

![Zustand und Verhalten eines Bankkontos](images/Bankkonto-ZustandUndVerhalten.jpeg)


Unterschiedliche Bankkonten teilen sich beispielsweise das Verhalten, dass man Geld einzahlen und abheben kann, das Guthaben (oder die Schulden) verzinst werden. Dieses Verhalten ist für alle Bankkonten gleich. Jedoch verfügt jedes einzelne Konto über individuelle Eigenschaften: Kontostand, Zinssatz, Gebühr oder Dispolimit können für jedes Konto variieren.

In der objektorientierten Programmierung (OOP) wird ein vereinfachter (_abstrahierter_) Bauplan aller Objekte geschaffen, der Verhalten und Zustand vereint: eine Klasse.

Eine Klasse beschreibt zum einen die Struktur, in der die  individuellen Eigenschaften eines jeden Objekts gespeichert werden (Kontostand, Inhaber-Name usw.). Dieser Zustand jedes einzelnen Objekts wird in Variablen gespeichert, die an das jeweilige Objekt geknüpft sind. Diese Variablen heißen in der OOP _Attribute_.

Zum anderen beschreibt eine Klasse das gemeinsame Verhalten aller Objekte in Funktionen. Diese Funktionen werden in der OOP _Methoden_ genannt. In der Regel ändern oder nutzen sie den Zustand des individuellen Objekts.

Nach dem Bauplan einer Klasse werden Objekte gebaut, die als konkrete _Instanz_ über einen eigenen Zustand verfügen. Objekte sind identifizierbare Ausprägungen eines Objekts. Mein Konto bei der Sparkasse ist ein konkretes Objekt der Klasse Konto: es verfügt über einen individuellen Kontostand usw. Objekte stellen eine Struktur dar, in der Verhalten und ein konkreter Zustand zusammengeführt werden.

### Eine Klasse entwerfen

Ein einfacher Bauplan für ein Konto könnte etwa so strukturiert sein:

![UML-Klassendiagramm für die Klasse Konto mit den Attributen inhaber, konto_stand und zinssatz sowie entsprechenden Methoden](plantuml/Konto-Klasse_allgemein.png)

Der Bauplan trägt den Namen `Konto` (oberes Kästchen). Ein konkretes benutzbares Konto umfasst seine individuellen Eigenschaften `inhaber_name`, `konto_stand` und `zinssatz` (Attribute, zweites Kästchen). Jedes Konto hat das selbe Verhalten `einzahlen`, `auszahlen`, `kontoausszug` und `verzinsen`.

### Eine Klasse mit Python implementieren

In Python wird eine Klasse implementiert, in dem eine neue Suite mit der folgenden Zeile eingeleitet wird:

```python
class Klassenname:
```

An der Stelle von `Klassenname` kann jeder beliebige Name stehen, dieser sollte in Python mit einem Großbuchstaben beginnen.

#### Methoden

Die Methoden werden als Python-Funktionen in der Suite der Klasse implementiert. Eine Besonderheit bei Python ist, dass Methoden in der Implementierung immer `self` als ersten Parameter entgegennehmen:

```python
def einzahlen(self, betrag: float) -> None:
    self.konto_stand += betrag
```

Die Referenz `self` weist auf das eigene Objekt, also die konkrete Instanz der Klasse. Über diese Referenz können die einzelnen Attribute angesprochen werden, bspw. `self.konto_stand`. Die Variable `self` kann in etwa so genutzt werden, wie in anderen Programmiersprachen (Java, C#, C++, PHP) die Referenz `this` bzw. `$this`.

Beim Aufruf der Methoden ist die Nennung des Arguments für `self` nicht  erforderlich. Soll beispielsweise auf das konkrete Konto `mein_konto` etwas eingezahlt werden, so wäre der Aufruf:

```python
mein_konto.einzahlen(100)
```

Warum `self` nur bei Implementierung genannt werden muss, jedoch nicht beim Aufruf lässt sich mit den internen Vorgängen erklären: Einen Aufruf von  `mein_konto.einzahlen(100)` wandelt der Interpreter um in  `Konto.einzahlen(mein_konto, 100)`.

#### Attribute (Instanzvariablen)

Alle Variablen, die Zustände des Objekts speichern sollen, müssen vor der ersten Benutzung initialisiert werden - also mit einem Startwert belegt sein. Damit nicht jede Programmiererin selbst dafür Sorge tragen muss, dass eine Methode mit der Initialisierung vor der ersten Nutzung der Attribute aufgerufen wird, gibt es in Python die Methode `__init__`, die immer bei Objekterzeugung aufgerufen wird. Diese Methode kann die benötigten Werte auch als Parameter übergeben bekommen.

```python
    def __init__(self, inhaber: str, zinssatz: float) -> None:
        self.konto_stand = 0
        self.inhaber = inhaber
        self.zinssatz = zinssatz
        self.kontoauszug_nr = 0
```

#### Objekterzeugung

Objekte werden erzeugt, in dem der Klassenname als Funktion aufgerufen wird. Die Anzahl und Art der benötigten Parameter wird in `__init__` (dem Konstruktor) festgelegt:

```python
mein_konto = Konto("Manu", 0.03)
dein_konto = Konto("Steffi", 0.01)
```
In unserem Beispiel erwartet die Klasse Konto einen Inhabernamen und einen Zinssatz, weil wir dies in der `__init__`-Methode so implementiert hatten.

Eine Klasse, die das oben beschriebene Verhalten und die Zustände beschreibt, könnte in etwa so aussehen:

```python
class Konto:
    def __init__(self, inhaber: str, zinssatz: float) -> None:
        self.konto_stand = 0
        self.inhaber = inhaber
        self.zinssatz = zinssatz
        self.kontoauszug_nr = 0

    def einzahlen(self, betrag: float) -> None:
        self.konto_stand += betrag

    def auszahlen(self, betrag: float) -> None:
        self.konto_stand -= betrag

    def kontoauszug(self) -> str:
        self.kontoauszug_nr += 1
        return "Konto von " + self.inhaber + ", Auszug " + str(self.kontoauszug_nr) + ", Betrag: " + str(self.konto_stand)

    def verzinsen(self) -> None:
        self.konto_stand +=  self.konto_stand * self.zinssatz
```

Aus dieser Klasse können Objekte instanziiert werden, die dann beispielsweise wie folgt benutzt werden können:

```python
mein_konto = Konto("Manu", 0.03)

mein_konto.einzahlen(100)
print(mein_konto.kontoauszug())

mein_konto.einzahlen(200)
print(mein_konto.kontoauszug())

mein_konto.verzinsen()
print(mein_konto.kontoauszug())

mein_konto.auszahlen(300)
print(mein_konto.kontoauszug())
```

Die Ausgabe wäre in diesem Fall:

```
Konto von Manu, Auszug 1, Betrag: 100
Konto von Manu, Auszug 2, Betrag: 300
Konto von Manu, Auszug 3, Betrag: 309.0
Konto von Manu, Auszug 4, Betrag: 9.0
```

### Nächste Schritte

Wo die Grundlagen der Objektorientierung schon gelegt sind, gehen wir den nächsten Schritt: [die Kapselung von Attributen in Python](https://oer-informatik.de/python_oop_kapselung).

### Weitere Literatur zu OOP/Klassen

- [Python-Dokumentation zu Objekten](https://docs.python.org/3/reference/datamodel.html)
