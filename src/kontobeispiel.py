
class Konto:
    def __init__(self, inhaber_name: str, zinssatz: float) -> None:
        self.konto_stand = 0
        self.inhaber_name = inhaber_name
        self.zinssatz = zinssatz
        self.kontoauszug_nr = 0

    def einzahlen(self, betrag: float) -> None:
        self.konto_stand += betrag

    def setIban(self) -> None:
        self.iban = "12345"

    def auszahlen(self, betrag: float) -> None:
        self.konto_stand -= betrag

    def kontoauszug(self) -> str:
        self.kontoauszug_nr += 1
        return "Konto von " + self.inhaber_name
    + ", Auszug " + str(self.kontoauszug_nr) + ", Betrag: "
    + str(self.konto_stand)

    def verzinsen(self) -> None:
        self.konto_stand += self.konto_stand * self.zinssatz


mein_konto = Konto("Manu", 0.03)
dein_konto = Konto("Steffi", 0.01)

mein_konto.einzahlen(100)
print(mein_konto.kontoauszug())
mein_konto.einzahlen(200)
print(mein_konto.kontoauszug())
mein_konto.verzinsen()
print(mein_konto.kontoauszug())
mein_konto.auszahlen(300)
print(mein_konto.kontoauszug())


dein_konto.einzahlen(100)

print(mein_konto.kontoauszug())
print(dein_konto.kontoauszug())

mein_konto.verzinsen()
dein_konto.verzinsen()

print(mein_konto.kontoauszug())
print(dein_konto.kontoauszug())

mein_konto.setIban()
print(mein_konto.iban)
