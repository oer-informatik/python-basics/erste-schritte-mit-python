import pygame
import time

class Background:
    def __init__(self, dot_size = 20, color=(0, 0, 0), 
                display_size = (600, 600)):
        self.__dot_size = dot_size
        self.__width, self.__height = display_size
        self.__col_nr, self.__row_nr = self.__width//dot_size, self.__height//dot_size
        self.__color = color
        self.__is_running = True
   
    def setup(self):
        pygame.init()
        self.__screen = pygame.display.set_mode([self.__width,self.__height])
        self.__dots=[]
        for col_id in range(self.__col_nr):
            row = []
            for row_id in range(self.__row_nr):
                row.append(True)
            self.__dots.append(row)
 
        self.__pacman = Pacman(step = self.__dot_size)     
        self.__ghost = Ghost(step = self.__dot_size-3, color=(0,255,255)) 

    def events(self):
        self.__is_running = True
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.__is_running = False
                return self.__is_running
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    self.__pacman.dir_up()
                if event.key == pygame.K_DOWN:
                    self.__pacman.dir_down()
                if event.key == pygame.K_RIGHT:
                    self.__pacman.dir_right()
                if event.key == pygame.K_LEFT:
                    self.__pacman.dir_left()
        return self.__is_running
    
    def update(self):
        pygame.time.delay(100)
        self.__ghost.move(self.__pacman)
        self.__pacman.update(self.__col_nr, self.__row_nr)
        
        self.consume_dot(self.__pacman.x_position, self.__pacman.y_position)
    
    def draw(self):
        self.__screen.fill((30, 30, 30))
    
        for col in range(self.__col_nr):
            for row in range(self.__row_nr):
                if self.__dots[col][row] == True:
                    pygame.draw.circle(self.__screen, (0, 0, 255), (int(self.__dot_size*(1/2 + col)), int(self.__dot_size*(1/2 + row))), self.__dot_size//4)
        self.__pacman.draw(self.__screen)
        self.__ghost.draw(self.__screen)
        pygame.display.flip()

    def quit(self):
        pygame.quit()
    
    def consume_dot(self, col, row):  
        self.__dots[col][row]= False
    
    @property
    def is_running(self) -> int:
        return self.__is_running



class Pacman:
    def __init__(self, radius=10, color=(255, 255, 0), step = 10):
        self.__x_position = 0
        self.__y_position = 0
        self.__dir_x = 0
        self.__dir_y = 0
        self.__radius = radius
        self.__color = color
        self.__step = int(step)

    def update(self, max_x, max_y):
        if (self.__y_position + self.__dir_y > max_y-1):
            self.__y_position = 0
        elif (self.__y_position + self.__dir_y < 0):
            self.__y_position = max_y-1
        else:
            self.__y_position = self.__y_position + self.__dir_y
        
        if (self.__x_position + self.__dir_x > max_x-1):
            self.__x_position = 0
        elif (self.__x_position + self.__dir_x < 0):
            self.__x_position = max_x-1
        else:
            self.__x_position = self.__x_position + self.__dir_x

    def dir_up(self):  
        self.__dir_y = -1
        self.__dir_x = 0

    def dir_down(self):  
        self.__dir_y = 1
        self.__dir_x = 0

    def dir_right(self):
        self.__dir_x = 1
        self.__dir_y = 0

    def dir_left(self):  
        self.__dir_x = -1
        self.__dir_y = 0
    
    def draw(self, screen): 
        pygame.draw.circle(screen, self.__color, (int(self.__step/2 +self.__x_position*self.__step), int(self.__step/2 +self.__y_position*self.__step)), self.__radius)

    @property
    def x_position(self) -> int:
        return self.__x_position

    @property
    def y_position(self) -> int:
        return self.__y_position


class Ghost:
    def __init__(self, radius=10, color=(255, 255, 0), step = 10):
        self.__x_position = 0
        self.__y_position = 0
        self.__radius = radius
        self.__color = color
        self.__step = int(step)

    def move(self, pacman):
        if (self.__y_position > pacman.y_position):
            self.__y_position += -0.5
        if (self.__y_position < pacman.y_position):
            self.__y_position += +0.5
        if (self.__x_position > pacman.x_position):
            self.__x_position += -0.5
        if (self.__x_position < pacman.x_position):
            self.__x_position += +0.5

    
    def draw(self, screen): 
        pygame.draw.circle(screen, self.__color, (int(self.__step/2 +self.__x_position*self.__step), int(self.__step/2 +self.__y_position*self.__step)), self.__radius)



background = Background()
background.setup()

while background.is_running:
    background.events()
    background.update()
    background.draw()
background.quit()
