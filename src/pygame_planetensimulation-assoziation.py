import pygame
import time
import math
from math import sin, cos


DISPLAY_HEIGHT = 600
DISPLAY_WIDTH = 600
is_running = True
TITLE = "Planetensimulation"

class Planet:

    def __init__(self, pygame, umlaufZeit=10, abstandZumZentrum=60, radius=10, color=(0, 0, 255), centerobject=None):
        self.__umlaufZeit = umlaufZeit
        self.__abstandZumZentrum = abstandZumZentrum
        self.__xZentrum = DISPLAY_WIDTH / 2
        self.__yZentrum = DISPLAY_HEIGHT / 2
        self.__xPosition = 0
        self.__yPosition = 0
        self.__radius = radius
        self.__color = color
        self.__startzeit = time.time()
        self.__pygame = pygame
        print("Center1 " + str(centerobject))
        self.__centerobject = centerobject
        if (self.__centerobject == None):
            print("Center2 " + str(centerobject))
            self.__xZentrum = DISPLAY_WIDTH / 2
            self.__yZentrum = DISPLAY_HEIGHT / 2

    @property
    def xPosition(self) -> int:
        return self.__xPosition

    @property
    def yPosition(self) -> int:
        return self.__yPosition
    
    def move(self):  # den Planeten bewegen
         # Umlaufwinkel aus Zeit und Umlaufzeit berechnen, dann die Koordinaten (Trigonomie)
        alpha = math.pi * (time.time()-self.__startzeit) / (self.__umlaufZeit)
        if type(self.__centerobject)==Planet:
            self.__xZentrum = self.__centerobject.xPosition
            self.__yZentrum = self.__centerobject.yPosition           
        #print("neues Zentrum ("+str(self.__centerobject.xPosition)+" / "+str(self.__centerobject.yPosition)+")")
        self.__xPosition = round(self.__xZentrum + self.__abstandZumZentrum * (-cos(alpha)))
        self.__yPosition = round(self.__yZentrum + self.__abstandZumZentrum * (-sin(alpha)))

    def draw(self):  # den Planeten zeichnen
        pygame.draw.circle(screen, self.__color, (self.__xPosition, self.__yPosition), self.__radius)

def setup():
    global screen
    pygame.init()
    screen = pygame.display.set_mode([DISPLAY_WIDTH,DISPLAY_HEIGHT])
    # Erzeugung eines Planeten. Die Parameter werden an Methode Planet.__init__() übergeben

    global planeten
    planeten = []
    #planeten.append(Planet(pygame, umlaufZeit=0.24, abstandZumZentrum=0.05*DISPLAY_WIDTH/2, radius=1, color=(153,77,0)))
    #planeten.append(Planet(pygame, umlaufZeit=0.61, abstandZumZentrum=0.06*DISPLAY_WIDTH/2, radius=3, color=(255,102,229)))
    erde = Planet(pygame, umlaufZeit=7, abstandZumZentrum=0.7*DISPLAY_WIDTH/2, radius=20, color=(0,0,255))
    planeten.append(erde)
    mond = Planet(pygame, umlaufZeit=1, abstandZumZentrum=0.1*DISPLAY_WIDTH/2, radius=7, color=(153,77,0), centerobject=erde)
    planeten.append(mond)
    #planeten.append(Planet(pygame, umlaufZeit=1.9, abstandZumZentrum=0.1*DISPLAY_WIDTH/2, radius=2, color=(255,0,0)))
    #planeten.append(Planet(pygame, umlaufZeit=11, abstandZumZentrum=0.3*DISPLAY_WIDTH/2, radius=20, color=(255,212,128)))
    #planeten.append(Planet(pygame, umlaufZeit=29, abstandZumZentrum=0.6*DISPLAY_WIDTH/2, radius=15, color=(179,117,0)))
    #planeten.append(Planet(pygame, umlaufZeit=84, abstandZumZentrum=0.8*DISPLAY_WIDTH/2, radius=8, color=(0,255,0)))
    #planeten.append(Planet(pygame, umlaufZeit=164, abstandZumZentrum=DISPLAY_WIDTH/2, radius=8, color=(80,80,255)))
 
def events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return False
    return True

def update():
    for planet in planeten:
        planet.move()

def draw():
    screen.fill((0, 0, 0))
    pygame.draw.circle(screen, (255, 255, 0), (DISPLAY_WIDTH//2, DISPLAY_HEIGHT//2), 40)
    for planet in planeten:
        planet.draw()
    pygame.display.flip()

setup()

while is_running:
    is_running = events()
    update()
    draw()
pygame.quit()
