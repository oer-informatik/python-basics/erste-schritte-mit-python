

class Konto:

    def __init__(self, iban: str, kontostand: float) -> None:
        self.__iban : str
        self.__iban = iban
        self.__kontostand : float
        self.__kontostand = kontostand        

    @property
    def iban(self) -> str:
         return self.__iban

    @iban.setter
    def iban(self, iban: str):
         self.__iban = iban

    @property
    def kontostand(self) -> float:
         return self.__kontostand

    @kontostand.setter
    def kontostand(self, kontostand: float):
         self.__kontostand = kontostand

class Privatkonto(Konto):

    def __init__(self, iban: str, kontostand: float, vorname: str, nachname: str) -> None:
        super().__init__(iban, kontostand)
        self.__vorname : str
        self.__vorname = vorname
        self.__nachname : str
        self.__nachname = nachname 
        print("IBAN: " +self.__iban)       

    @property
    def vorname(self) -> str:
         return self.__vorname

    @vorname.setter
    def vorname(self, vorname: str):
         self.__vorname = vorname

    @property
    def nachname(self) -> str:
         return self.__nachname

    @nachname.setter
    def nachname(self, nachname: str):
         self.__nachname = nachname
         
mein_konto = Konto("DE123", 0.0)
print(mein_konto.iban)
mein_konto.kontostand =100.0
print(mein_konto.kontostand)

mein_privatkonto = Privatkonto("DE124", 1.0, "hannes", "mustermann")
print(mein_privatkonto.iban)
print(mein_privatkonto.vorname)
mein_privatkonto.kontostand =123.0
print(mein_privatkonto.kontostand)
