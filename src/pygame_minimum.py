import pygame

DISPLAY_HEIGHT = 200
DISPLAY_WIDTH = 300
is_running = True
speed_x, speed_y = 0.1, 0.1 
pos_x, pos_y = 1, 1

    
def setup():
    global screen
    pygame.init()
    screen = pygame.display.set_mode([DISPLAY_WIDTH,DISPLAY_HEIGHT])


def events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return False
    return True

def update():
    global pos_x
    global pos_y
    global speed_x
    global speed_y
    pos_x += speed_x
    pos_y += speed_y
    if (pos_x>DISPLAY_WIDTH) or (pos_x<=0) : speed_x *= -1
    if (pos_y>DISPLAY_HEIGHT) or (pos_y<=0) : speed_y *= -1

def draw():
    screen.fill((0, 0, 0))
    pygame.draw.circle(screen, (255, 255, 0), (int(pos_x), int(pos_y)), DISPLAY_WIDTH//20)
    pygame.display.flip()



setup()

while is_running:
    is_running = events()
    update()
    draw()
pygame.quit()
