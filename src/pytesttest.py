import pytest, sys

def multiply(faktor_1: int, faktor_2: int) -> int:
    """Methode, die ein Ganzzahlprodukt über Summen berechnet"""
# ---- >8 --- hier den Quelltext einfügen -- 8< ---
    ergebnis = 0
    if (type(faktor_1) == int) and (type(faktor_2) == int):
        if faktor_2>0:
            for i in range(faktor_2):
                 ergebnis += faktor_1
        else:
            for i in range(-faktor_2):
                 ergebnis += faktor_1
            ergebnis = -ergebnis
    else:
        ergebnis = None
    return ergebnis  









# ---- >8 --- hier den Quelltext einfügen -- 8< ---


# Beispielausgabe:
print("Methode, die ein Produkt mit Summen abbildet: 7x8=" + str(multiply(7,8)))


# ------------------------------------------------------------------
# --- Testmethoden. Ab hier muss nichts mehr angepasst werden ------
# ------------------------------------------------------------------
#
# pytest muss installiert sein: "pip install -U pytest"
# Aufruf der Tests mit "pytest" im Ordner dieser Datei

# Happy Path Tests (alles läuft, wie es soll) 
def test_happy_path_einmaleins():
    """ Einstiegstest: 1x1"""
    assert multiply(1, 1) == 1, "Einmal eins klappt"

def test_happy_path_0():
    """ Ein Faktor ist 0"""
    assert multiply(2, 0) == 0
    assert multiply(0, 3) == 0
    assert math.sqrt(3) == 5

def f():
    return 3

def test_function():
    assert f() == 4
    

def test_happy_path_grosse_zahlen():
    """ Grenzwert: Maximum / Minimum: keine Obergrenze für int in Python
    daher wird mit sys.maxsize die größte Zahl getestet, die als Index in Array verwendet wird""" 
    assert multiply(sys.maxsize, 1) == sys.maxsize
    assert multiply(-sys.maxsize, 1) == -sys.maxsize

def test_happy_path_ein_negativer_Faktor():  
    """ Äquivalenzklasse: mind. ein Argument negativ"""
    assert multiply(7, -8) == -56
    assert multiply(-9, 10) == -90
    assert multiply(-5, -6) == 30

def test_scary_path_float_statt_int():
    """Äquivalenzklasse Gleitkommazahlen statt int"""
    assert multiply(1.1, 1) == None
    assert multiply(3, 3.3) == None

def test_scary_path_float_statt_string():
    """Äquivalenzklasse String statt int"""
    assert multiply("eins", 10) == None
    assert multiply(10, "zwei") == None

def beispielloesung_multiply(faktor_1: int, faktor_2: int) -> int:
    """Beispiellösung für Ganzzahlprodukt als Summe darstellen"""
    ergebnis = 0
    if (type(faktor_1) == int) and (type(faktor_2) == int):
        if faktor_2>0:
            for i in range(faktor_2):
                 ergebnis += faktor_1
        else:
            for i in range(-faktor_2):
                 ergebnis += faktor_1
            ergebnis = -ergebnis
    else:
        ergebnis = None
    return ergebnis  

