print("Hello, ")
print("World!")


print("Hello, ", end="\n")
print("World!")


print("Hello, ", end="")
print("World!")


import sys

sys.stdout.write("Hello, ")
sys.stdout.write("World!")


text1 = "Hello"
text2 = "World"
text3 = text1 + text2
print(text3)


print('Zeilenumbruch\nhier')
print("und auch\nhier")
print('Backslash\\')
print("noch einer:\\")
print(" \tA\tB\tC")
print("1\tA1\tB1\tC1")
print("2\tA2\tB2\tC2")
print("3\tA3\tB3\tC3")

print("Programm" + "Aufruf")


print("Da"*3)

print("Die Zahl fünf schreibt sich so: " + str(5))


vorname = "Martina"
nachname = "Mustermann"
print ("Sehr geehrte/r %s %s"% (vorname,nachname))

anrede= "Frau"
print ("Sehr geehrte {} {} {}!".format(anrede, nachname, vorname))
print ("Sehr geehrte {title} {surname} {lastname}, liebe {surname}!".format(lastname=nachname, surname=vorname, title=anrede))
print ("Sehr geehrte {2} {1} {0}, liebe {1}!".format(nachname, vorname, anrede))


snare=["tschack tschack", "tschak", "tschack tschack", "tschackalak"]
bassdrum=" BUMM "
beatbox = bassdrum.join(snare)
print(bassdrum + beatbox)

zahl = 1
text = "Fehler tritt hier nicht auf"

print(zahl)
print(text)
print(zahl + text)

grade = input("What is the password?")
if (grade == "xray"):
    print ("Password entered successfully.")
else:
    print ("Incorrect password.")
