import pygame
import time
import math
from math import sin, cos

class Background:
    def __init__(self, color=(0, 0, 0), 
                display_size = (600, 600)):
        self.__width, self.__height = display_size
        self.__color = color
        self.__is_running = True
   
    def setup(self):
        pygame.init()
        self.__screen = pygame.display.set_mode([self.__width,self.__height])
 
        self.__planeten = []
        erde = Planet(pygame, umlaufzeit=7, umlaufbahn_radius=0.7*self.__height/2, radius=20, color=(0,0,255), centerobject=self)
        self.__planeten.append(erde)
        mond = Planet(pygame, umlaufzeit=1, umlaufbahn_radius=0.1*self.__height/2, radius=7, color=(153,77,0), centerobject=erde)
        self.__planeten.append(mond) 
        
    def events(self):
        self.__is_running = True
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.__is_running = False
                return self.__is_running
        return self.__is_running
    
    def update(self):
        pygame.time.delay(10)
        for planet in self.__planeten:
            planet.move()
         
    def draw(self):
        self.__screen.fill((30, 30, 30))
        for planet in self.__planeten:
            planet.draw(self.__screen)
        
        pygame.display.flip()

    def quit(self):
        pygame.quit()
    
    @property
    def x_position(self) -> int:
        return self.__width/2

    @property
    def y_position(self) -> int:
        return self.__height/2
    @property
    def is_running(self) -> bool:
        return self.__is_running

    
class Flugobjekt:

    def __init__(self, pygame, size=10,  radius=10, color=(0, 0, 255), centerobject=None):
        self.__x_position = 0
        self.__y_position = 0
        self.__size = size
        self.__color = color
        self.__startzeit = time.time()
        self.__pygame = pygame
        self.__centerobject = centerobject

    @property
    def x_position(self) -> int:
        return self.__x_position

    @property
    def y_position(self) -> int:
        return self.__y_position
    
    def move(self):  # den Planeten bewegen
        pass

    def draw(self, screen):  # den Planeten zeichnen
        self.__pygame.draw.circle(screen, self.__color, (self.__x_position, self.__y_position), self.__radius)

    @property
    def startzeit(self) -> int:
        return self.__startzeit

    @property
    def centerobject(self) -> int:
        return self.__centerobject

    @centerobject.setter
    def centerobject(self, centerobject):
         self.__centerobject = centerobject

class Planet(Flugobjekt):

    def __init__(self, pygame, umlaufzeit=10, umlaufbahn_radius=60, radius=10, color=(0, 0, 255), centerobject=None):
        super().__init__(self, pygame)
        self.__umlaufzeit = umlaufzeit
        self.__umlaufbahn_radius = umlaufbahn_radius
        self.centerobject = centerobject

    def move(self):  # den Planeten bewegen
        # Umlaufwinkel aus Zeit und umlaufzeit berechnen, dann die Koordinaten (Trigonomie)
        alpha = math.pi * (time.time()-self.startzeit) / (self.__umlaufzeit)
        self.__x_zentrum = self.centerobject.x_position
        self.__y_zentrum = self.centerobject.y_position           
        self.__x_position = round(self.__x_zentrum + self.__umlaufbahn_radius * (-cos(alpha)))
        self.__y_position = round(self.__y_zentrum + self.__umlaufbahn_radius * (-sin(alpha)))

    def draw(self, screen):  # den Planeten zeichnen
        self._Flugobjekt__pygame.draw.circle(screen, self.__color, (self.__x_position, self.__y_position), self.__radius)


class Rakete(Flugobjekt):

    def __init__(self, pygame, umlaufzeit=10, umlaufbahn_radius=60, radius=10, color=(0, 0, 255), centerobject=None):
        super().__init__(self, pygame,  size=10, color=(0, 0, 255), centerobject=None)
        super().x_position = centerobject.x_position
        super().y_position = centerobject.x_position

    def update(self, max_x, max_y):
        if (self.__y_position + self.__dir_y > max_y-1):
            self.__y_position = 0
        elif (self.__y_position + self.__dir_y < 0):
            self.__y_position = max_y-1
        else:
            self.__y_position = self.__y_position + self.__dir_y
        
        if (self.__x_position + self.__dir_x > max_x-1):
            self.__x_position = 0
        elif (self.__x_position + self.__dir_x < 0):
            self.__x_position = max_x-1
        else:
            self.__x_position = self.__x_position + self.__dir_x


    def draw(self, screen):  # den Planeten zeichnen
        self._Flugobjekt__pygame.draw.circle(screen, self.__color, (self.__x_position, self.__y_position), self.__radius)


background = Background()
background.setup()

while background.is_running:
    background.events()
    background.update()
    background.draw()
background.quit()
