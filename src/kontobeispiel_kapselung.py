from typing import List, ClassVar


class Konto:
    konto_stand: float
    inhaber_name: str
    zinssatz: float
    kontoauszug_nr: int
    iban: str
    konten: ClassVar[List['Konto']] = []

    def __init__(self, inhaber_name: str, zinssatz: float) -> None:
        self.konto_stand = 0
        self.inhaber_name = inhaber_name
        self.zinssatz = zinssatz
        self.kontoauszug_nr = 0
        self.iban = ""
        Konto.konten.append(self)

    def einzahlen(self, betrag: float) -> None:
        self.konto_stand += betrag

    def set_iban(self) -> None:
        self.iban = "12345"

    def auszahlen(self, betrag: float) -> None:
        self.konto_stand -= betrag

    def kontoauszug(self) -> str:
        self.kontoauszug_nr += 1
        return "Konto von " + self.inhaber_name + ", Auszug " + str(self.kontoauszug_nr) + ", Betrag: " + str(self.konto_stand)

    def verzinsen(self) -> None:
        self.konto_stand += self.konto_stand * self.zinssatz

    @staticmethod
    def konten_uebersicht() -> str:
        uebersicht = ''
        for ein_konto in Konto.konten:
            uebersicht += ein_konto.kontoauszug() + "\n"
        return uebersicht


mein_konto = Konto("Manu", 0.03)
dein_konto = Konto("Steffi", 0.01)

mein_konto.einzahlen(100)
print(mein_konto.kontoauszug())
mein_konto.einzahlen(200)
print(mein_konto.kontoauszug())
mein_konto.verzinsen()
print(mein_konto.kontoauszug())
mein_konto.auszahlen(300)
print(mein_konto.kontoauszug())


dein_konto.einzahlen(100)

print(mein_konto.kontoauszug())
print(dein_konto.kontoauszug())

mein_konto.verzinsen()
dein_konto.verzinsen()

print(mein_konto.kontoauszug())
print(dein_konto.kontoauszug())

mein_konto.set_iban()
print(mein_konto.iban)

print("Uebersicht:")
print(Konto.konten_uebersicht())