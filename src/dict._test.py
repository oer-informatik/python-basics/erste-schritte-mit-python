import pytest
from typing import Dict

print('=======\nErstellen eines neuen Dictionarys:')
person = {'Vorname': 'Horst', 'Nachname': 'Müller'}
print(person)

print('\n=======\nDatentyp:')
print(type(person))

print('\n=======\nÄnderung des Namens:')
person['Vorname'] = 'Herbert'
print(person['Vorname'])
print(person)

print('\n=======\nHinzufügen eines Wertpaares:')
person['Alter'] = 44

print('\n=======\nIteration 1')
for key in person:
    print(key)

print('\n=======\nIteration 2 (sortiert)')
for key in sorted(person):
    print(key)

print('\n=======\nIteration 3 (key: value, sortiert)')

for key in sorted(person):
    print(key + ": " + str(person[key]))

print('\n=======\nIteration 4 (key: value, sortiert)')
for key, value in sorted(person.items()):
    print(key + ": " + str(value))

print('\n=======\nÜberprüfen, ob Werte vorhanden sind:')
if 'Adresse' not in person:
    print('\n=======\nKeine Adresse erfasst')
else:
    print('\n=======\nDie Adresse lautet' + str(person['Adresse']))

print('\n=======\nÜberprüfen, ob Werte vorhanden sind')
print('mit Alternativer Wertausgabe:')
print("Die Adresse lautet "+person.get('Adresse', 'N/A'))

print('\n=======\nNach Wertzuweisung')
person['Adresse'] = 'Musterstraße 12, 12345 Musterstadt'
print("Die Adresse lautet "+person.get('Adresse', 'N/A'))

print('\n=======\nZugriff auf nicht vorhandene Schlüssel abfangen')
if 'Kinder' not in person:
    person['Kinder'] = 0
person['Kinder'] += 1
print(person)

print('\n=======\nDictionary mit Type Annotation')
konto: Dict[str, int] = {}
konto['iban'] = 12345
print(konto)

artikelpreise: Dict[int, float] = {665544: 12.23, 112233: 1.99}
print(artikelpreise)

def get_person(first_name: str, last_name: str) -> Dict[str, str]:
    """Wandle die übergebenen Werte in ein personen-dict um"""
    person = {"Vorname": first_name, "Nachname": last_name}
    return person

print(get_person("Hubert", "Schmidt"))

print('\n=======\nType Annotation sind nur Kommentare => missachtbar')
konto['bic'] = "DE1234"
print(konto)
artikelpreise["Hose"] = 89.99
print(artikelpreise)

