import pygame
import time
import math
from math import sin, cos


DISPLAY_HEIGHT = 600
DISPLAY_WIDTH = 600
is_running = True
TITLE = "Planetensimulation"

class Planet:

    def __init__(self, pygame, umlaufZeit=10, abstandZumZentrum=60, radius=10, color=(0, 0, 255)):
        self.umlaufZeit = umlaufZeit
        self.abstandZumZentrum = abstandZumZentrum
        self.xZentrum = DISPLAY_WIDTH / 2
        self.yZentrum = DISPLAY_HEIGHT / 2
        self.xPosition = 0
        self.yPosition = 0
        self.radius = radius
        self.color = color
        self.startzeit = time.time()
        self.pygame = pygame

    def move(self):  # den Planeten bewegen
        # Umlaufwinkel aus Zeit und Umlaufzeit berechnen, dann die Koordinaten (Trigonomie)
        alpha = math.pi * (time.time()-self.startzeit) / (self.umlaufZeit)
        self.xPosition = round(self.xZentrum + self.abstandZumZentrum * (-cos(alpha)))
        self.yPosition = round(self.yZentrum + self.abstandZumZentrum * (-sin(alpha)))

    def draw(self):  # den Planeten zeichnen
        pygame.draw.circle(screen, self.color, (self.xPosition, self.yPosition), self.radius)

def setup():
    global screen
    pygame.init()
    screen = pygame.display.set_mode([DISPLAY_WIDTH,DISPLAY_HEIGHT])
    # Erzeugung eines Planeten. Die Parameter werden an Methode Planet.__init__() übergeben

    global planeten
    planeten = []
    planeten.append(Planet(pygame, umlaufZeit=0.24, abstandZumZentrum=0.05*DISPLAY_WIDTH/2, radius=1, color=(153,77,0)))
    planeten.append(Planet(pygame, umlaufZeit=0.61, abstandZumZentrum=0.06*DISPLAY_WIDTH/2, radius=3, color=(255,102,229)))
    planeten.append(Planet(pygame, umlaufZeit=1, abstandZumZentrum=0.08*DISPLAY_WIDTH/2, radius=3, color=(0,0,255)))
    planeten.append(Planet(pygame, umlaufZeit=1.9, abstandZumZentrum=0.1*DISPLAY_WIDTH/2, radius=2, color=(255,0,0)))
    planeten.append(Planet(pygame, umlaufZeit=11, abstandZumZentrum=0.3*DISPLAY_WIDTH/2, radius=20, color=(255,212,128)))
    planeten.append(Planet(pygame, umlaufZeit=29, abstandZumZentrum=0.6*DISPLAY_WIDTH/2, radius=15, color=(179,117,0)))
    planeten.append(Planet(pygame, umlaufZeit=84, abstandZumZentrum=0.8*DISPLAY_WIDTH/2, radius=8, color=(0,255,0)))
    planeten.append(Planet(pygame, umlaufZeit=164, abstandZumZentrum=DISPLAY_WIDTH/2, radius=8, color=(80,80,255)))
 
def events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return False
    return True

def update():
    for planet in planeten:
        planet.move()

def draw():
    screen.fill((0, 0, 0))
    pygame.draw.circle(screen, (255, 255, 0), (DISPLAY_WIDTH//2, DISPLAY_HEIGHT//2), 5)
    for planet in planeten:
        planet.draw()
    pygame.display.flip()


setup()

while is_running:
    is_running = events()
    update()
    draw()
pygame.quit()
