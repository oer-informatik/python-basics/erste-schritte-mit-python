## Funktionen in Python

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_functions</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Komplexe Probleme lassen sich am besten lösen, in dem sie in kleine Einheiten aufgeteilt werden, die jeweils ein Teilproblem lösen. Neben den Suites stellen in Python Funktionen die wesentliche Struktur dar, um die Wiederverwertbarkeit, Wartbarkeit und Änderbarkeit von Code sicherzustellen. Hier geht es um Positions- und Keyword-Argumente, Parameter, Defaultwerte, Dokumentation und all' die anderen Grundlagen von Funktionen._

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Grundüberlegungen zur Aufteilung von Code

In der Programmierung verfolgen wir Ziele, die uns nötigen, den Quelltext in mehrere Abschnitte aufzuteilen:

* Langer Code ist schwer lesbar. Man sagt, dass guter Code in Abschnitte gegliedert sein soll, der mit einem Blick erkennbar ist - manche sprechen hier sogar von 20 oder weniger Codezeilen. Code, der diesen Regeln folgt, gilt als gut wartbar und erweiterbar.

* Große Aufgaben sollten in kleine Pakete zerteilt werden, damit die kleinen Portionen leichter testbar sind. Ein kleinerer Codeblock, der nur eine einzige Aufgabe erfüllt, kann mit deutlich weniger Testfällen gut getestet werden. Die Erfüllung einer Aufgabe lässt sich leichter testen als die verschlungene Umsetzung vieler Aufgaben.

* Code, der in mehrere Abschnitte aufgeteilt wird, kann auch arbeitsteilig von mehreren Entwickler*innen implementiert werden.

Die unterste Einheit, um Codeblöcke in Abschnitte zu zerteilen, sind Funktionen. Funktionen erzielen Effekte (Wirkungen), in dem sie das E.V.A.-Prinzip umsetzen: **E**ingabe, **V**erarbeitung, **A**usgabe:

* Die **E**ingabe kann über Werte erfolgen, die an die Funktion übergeben werden (z.B. die Zeichenkette "Hallo Welt" in  `print("Hallo Welt")`). Im Aufruf der Funktion werden diese Werte _Argumente_ genannt. Diese Werte werden an die Implementierung der Funktion weitergegeben über Variablen, in denen sie gespeichert werden. Diese Variablen werden _Parameter_ genannt. Parameter werden in der Definition der Funktion in der _Signatur_ (der Kopfzeile der Funktion) hinter dem Funktionsnamen in Klammern notiert.

* Die **V**erarbeitung erfolgt in dem Funktionskörper (_body_). Dieser beginnt eingerückt in der Folgezeile des Funktionskopfs und endet, sobald wieder das Einrückungsmaß der Kopfzeile erreicht wird.

* Die **A**usgabe erfolgt über das `return`-Keyword. Hinter `return` wird der Wert notiert, der an den aufrufenden Codeblock übergeben wird. Mit dem Erreichen eines `return`-Statements endet die Ausführung der Funktion - unabhängig davon, ob darauf noch Code folgt. Mit dem übergebenen Wert wird auch die der Kontrollfluss wieder an die Codesequenz übergeben, aus der heraus die Funktion aufgerufen wurde. Wenn die Funktion keinen Wert übergeben soll, kann das `return`-Statement auch ohne Rückgabewert notiert werden: dann veranlasst es lediglich das Verlassen der Funktion an diesem Punkt. Man sagt: der Kontrollfluss wird an die Aufrufende Methode zurückgegeben.

### Basisaufbau einer Funktion

Die Funktionsdefinition sieht also in etwa so aus:

Ohne Parameter und ohne Rückgabewert (`pass` ist eine Art Platzhalter der nötig ist so lange noch kein Code vorhanden ist):

```python
def funktions_name():
  pass
```

Ohne Parameter und mit Rückgabewert:

```python
def funktions_name():
  return "Hallo"
```

Mit Parameter und ohne Rückgabewert:

```python
def funktions_name(name):
  print("Hallo", name)
```

(Diese Funktion hat zwar keinen Rückgabewert - der aufrufende Codeblock erhält also kein Ergebnis zurück. Dennoch verfügt diese Methode über einen _Effekt_: es wird eine Ausgabe auf der Konsole erzeugt.)

Ein einfaches praktisches Beispiel ist hier ergänzt um _Type Annotations_ (Datentyp-Kommentierung hinter dem Doppelpunkt der Parameter und hinter dem stilisierten Pfeil für den Rückgabewert):

```python
def hello_world(name: str) -> str:
  """Begrüßt den Entwickler freundlich"""
  return "Hallo "+name+"!"
```

Damit die so definierte Funktion auch ausgeführt wird, muss sie noch aufgerufen (_invoke_) werden. Dies darf erst unterhalb der Funktionsdefinition erfolgen (sonst wird die Funktion nicht gefunden) und sieht dann beispielsweise so aus:

```python
print(hello_world("Hannes"))
```

Der Rückgabewert der Funktion tritt an die Stelle im Code, an der der Funktionsaufruf war. In diesem Beispiel würde also "Hallo Hannes!" auf der Konsole ausgegeben.


### Zusammengefasst: Was kennzeichnet Funktionen in Python?

- Sie können Werte (Argumente) entgegennehmen und stellen diese intern in Parameter-Variablen zur Verfügung.

- Sie können Variablen und Werte auswerten und ggf. ein Ergebnis zurückgeben.

- Sie können einen Effekt haben (Ausgaben, Dateien erzeugen, Klänge abspielen...).

- Man kann den Aufruf einer Funktion immer an den auf den Namen folgenden Klammern erkennen.

 - Die Implementierung einer Funktion wird immer mit dem Schlüsselwort `def` eingeleitet.

### Welche Arten von Funktionen gibt es in Python?

- build-in (Python): interne Python Funktionen (z.B. `print()`)^[Alle internen Python-Funktionen finden sich hier [im Python Doc](https://docs.python.org/3/library/functions.html)],

- Funktionen aus Python-Modulen (die per `import` eingefügt werden),

- selbst implementierte Funktionen/Methoden,

- lambda-Funktionen (dazu an anderer Stelle mehr).

### Was sind Parameter, was sind Argumente?

Beim Aufruf einer Funktion können Werte übergeben werden.

Diese werden _bei Funktionsaufruf_ in Klammern geschrieben und dort _Argumente_ genannt. Ein Beispiel ist das `"Hallo"` in diesem Aufruf:

```python
hello_world("Hallo")
```

Die Argumentwerte (`"Hallo"`) werden innerhalb der Funktion in Variablen gespeichert, die _Parameter_ genannt werden:

```python
def hello_world(name: str):
```

Die Variable `name` ist hier der Parameter, der im obigen Aufruf den Argumentwert `"Hallo"` zugewiesen bekommt.

Diese Parameter sind nur innerhalb der Funktion gültig. Parameterwerte können auf zweierlei Art zugewiesen werden: anhand der Position und anhand des Namens.

#### Argumente anhand der Parameter-Position festlegen (_positional argument passing_)

Am Beispiel einer kleinen Funktion sollen die unterschiedlichen Arten demonstriert werden, wie Argumente bestimmten Parametern zugeordnet werden können:

```python
def hello_world(name: str, begruessung: str):
  """Begrüßt den Entwickler freundlich"""
  print(begruessung, name)
```

Der erste und einfachste Weg sind die Positionsparameter: Das erste Argument wird dem ersten Parameter zugeordner, das zweite dem zweiten usw.:

```python  
hello_world("Gustl", "Servus")
hello_world("Schlafmütze", "Guten Morgen")
```

> Servus Gustl
> Guten Morgen Schlafmütze

#### Argumente anhand der Parameter-Namen festlegen (_keyword_ oder _named-argument passing_)

Gerade bei Methoden mit vielen Parametern kann es sinnvoll sein, die Parameter über die Namen anzusprechen: so wird direkt klar, welches Argument zu welchem Parameter gehört. Hierbei kann die Reihenfolge auch vertauscht werden, da zur Identifizierung alleine der Name genutzt wird:

```python
hello_world(name = "Herr Pfarrer", begruessung = "Grüß Gott")
```

> Grüß Gott Herr Pfarrer

Position-Argumente müssen immer vor Keyword-Argumenten übergeben werden:

```python
hello_world("Herr Pfarrer", begruessung="Grüß Gott") # geht
hello_world(name="Herr Pfarrer", "Grüß Gott")        # geht nicht!
```

Im Fall von  `print()` kann man die Kombination aus Positions- und Keyword-Parametern erkennen: Alle Parameter nach `objects` sind Keyword-Parameter.

```python
print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
```

#### Optionale Parameter erstellen

Im Funktionskopf kann für jeden Parameter ein Default-Wert übergeben werden. Wenn ein solcher vergeben wurde, ist es nicht mehr erforderlich, für diesen Parameter ein Argument anzuhängen. Das Argument kann aber übergeben werden, wenn vom Defaultwert abgewichen werden soll:

```python
def hello_world(name: str, begruessung : str = "Hallo"):
  """Begrüßt den Entwickler freundlich"""
  print(begruessung, name)
```

```python
hello_world(name = "Spencer")
hello_world("Welt")
hello_world("Sonnenschein", "Guten Morgen")
```

Jedoch muss aufgepasst werden, dass keinesfalls ein Argument zweimal übergeben wird: also Positions- und als Keyword-Parameter:

```python
hello_world("Maier" , name = "Meyer")
```

In der Hilfe (`help()`) werden alle _position-only_ Parameter vor dem _Slash_ notiert:

```python
def foo(p1, p2, /, p3, p4)
```

### Funktionen ohne Rückgabewert

Funktionen ohne Rückgabewert geben `None` zurück. Wenn das Ende der Funktion erreicht ist, und dort kein `return`-Statement steht, wird implizit vom Interpreter ein

```python
  return None
```

ergänzt. Beispielhaft erzeugt folgender Programmcode...

```Python
def ich_tue_nichts():
    pass

print(ich_tue_nichts())
```

...diese Ausgabe:

> None

Statements hinter dem `return` werden nicht mehr erreicht:

```python
def ich_tue_nichts():
    print(1)
    print(2)
    print(3)
    return
    print(4)

ich_tue_nichts()
```

### Dokumentieren von Funktionen

Da Python eine dynamisch typisierende Sprache ist, ist nicht immer leicht verständlich, welche Werte für einzelne Variablen erwartet werden. Das gilt insbesondere bei Parametern und Rückgabewerten. Es ist daher sinnvoll, die Datentypen dieser Variablen zu annotieren.

Außerdem sollte für jede Funktion in einer kurzen Beschreibung genannt werden, was sie tut. Im Idealfall ist bereits der Name der Funktion so aussagekräftig, dass in der Kommentierung danach (in dreifachen Anführungszeichen) nur ergänzende Informationen gegeben werden müssen.

```python
def summe(a:int, b:int=0) -> int:
    """Addiert die Werte"""
    c=a+b
    print("Berechne die Summe aus", a, "und", b, ": ", c)
    return c
```

Wenn die Methode so dokumentiert ist, kann diese Information auch über die `help()`-Methode ausgelesen werden:

```
>>> help(summe)
Help on function summe in module __main__:

summe(a: int, b: int = 0) -> int
    Addiert die Werte
```


### Besonderheit: Funktionen, die eine variable Anzahl an Argumenten übergeben bekommen

Es gibt eine Reihe weiterer Besonderheiten von Funktionen, auf die an anderer Stelle eingegangen wird:

* Funktionen können auch eine variable Anzahl an Argumenten (oder Keyword-Argumenten) entgegennehmen (z.B. kann man der Print-Funktion mehere Argumente übergeben, die in dem Tupel `objects` gespeichert werden:  `python
print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)`),

* Funktionen können auch eine Variable Anzahl an Rückgabewerten haben (`return (a, b)`).


### Leitfragen

 - Wie ist der allgemeine Aufbau einer Funktion/Methode in Python?

 - Was unterscheidet Argumente von Parametern?

 - Was sind benannte Parameter und Positionsparameter?

 - Wie erstelle ich optionale Parameter und Default-Werte?

 - Wie erstelle ich Funktionen mit einer variablen Anzahl an Parametern?

 - Wie erstelle ich Funktionen mit einer variablen Anzahl an benannten Parametern?

 - Wie erstelle ich eine Funktion mit einem/mehreren Rückgabewerten?

 - Wie kommentiere/dokumentiere ich eine Funktion?

### Fazit

Funktionen sind schon ein ziemlich dickes Brett, jedenfalls dann, wenn wir uns die unterschiedlichen Parametervarianten anschauen und sie sauber kommentieren wollen.

### Nächste Schritte

Als Nächstes wollen wir uns einem zentralen Datentypen von Python zuwenden: dem [Dictionary](https://oer-informatik.de/python_dictionary)


### Links und weitere Informationen

- Alle internen Python-Funktionen finden sich hier [im Python Doc](https://docs.python.org/3/library/functions.html)

