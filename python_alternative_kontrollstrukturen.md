## Alternative Kontrollstrukturen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_alternative_kontrollstrukturen</span>


> **tl/dr;** _(ca. 12 min Lesezeit): Wer die Wahl hat, hat die Qual: mit den alternativen Kontrollstrukturen (If-Statements) halten die Suites (Einrückungen) erstmals Einzug in Python. Und die Enttäuschung, weil Mehrfachauswahlen in Python recht umständlich realisiert werden müssen._

Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Dictionary](https://oer-informatik.de/python_dictionary)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)


### Bedingte Anweisungen

In bedingten Anweisungen wird überprüft, ob ein Ausdruck (_expression_) als `True` ausgewertet wird. Nur wenn diese Bedingung zutrifft, wird der Code in der eingerückten _Suite_ (Codeblock) ausgeführt:

```python
temperatur = int(input("Wie warm ist es (in Grad Celsius)?"))

if temperatur > 20:
  print("Auf in's Freibad!")   # wird nur ausgeführt, wenn Bedingung zutrifft)
print("Danke für die Info!")   # wird in jedem Fall ausgeführt
```

![Struktogramm zu einfacher bedingten Anweisung if (Temperatur > 20) "Auf in's Freibad"](images/bedingteAnweisung1.PNG)

Neben der _Suite_, die ausgeführt wird, wenn die Bedingung zutrifft, gibt es auch noch einen Bereich, der nur dann ausgeführt wird, wenn die Bedingung nicht zutrifft (`else`):

```python
if temperatur > 20:
  print("Auf in's Freibad!")          # wird nur ausgeführt, wenn Bedingung zutrifft)
else:
  print("Schade, nicht warm genug.")  # wird inur ausgeführt, wenn Bedingung nicht zutrifft.
print("Danke für die Info!")          # wird in jedem Fall ausgeführt
```

![Struktogramm zu bedingten Anweisung mit else: if (Temperatur > 20) "Auf in's Freibad" else "Schade"](images/bedingteAnweisung2.PNG)


Zwischen mehr als zwei Alternativen wählen zu können, muss man in Python ein `if/elif/else`- Konstrukt wählen. Anders als in anderen Programmiersprachen gibt es hier kein switch/case:

```python
if temperatur > 20:
  print("Auf in's Freibad!")           # wird nur ausgeführt, wenn Bedingung zutrifft)
elif temperatur <0:
  print("Wie wäre es mit Ski fahren?") # wird nur ausgeführt, wenn die erste Bedingung nicht, die zweite aber schon zutrifft.
else:
  print("Schade, nicht warm genug.")   # wird inur ausgeführt, wenn Bedingung nicht zutrifft.
print("Danke für die Info!")           # wird in jedem Fall ausgeführt
```

Hierbei ist zu beachten, dass immer nur eine _Suite_ ausgeführt wird: trifft die erste Bedingung zu, so wird nur die zugehörige Suite ausgeführt - alle weiteren Bedingungen werden ignoriert. Als Beispiel:

```python
if temperatur > 20:
  print("warm") # wird nur ausgeführt, wenn Bedingung zutrifft)
elif temperatur >25:
  print("sehr warm") # wird nur ausgeführt, wenn die erste Bedingung nicht, die zweite aber schon zutrifft.
else:
  print("oder?") # wird nur ausgeführt, wenn Bedingung nicht zutrifft.
print("!")            # wird in jedem Fall ausgeführt
```

Auch wenn sich die Bedingungen `temperatur > 20` und `temperatur >25` überschneiden: bei einer Temperatur von 26 wird lediglich der erste Block ausgeführt.

### Kein `switch`/`case` in Python

Mehrfachauswahlen lassen sich in Python nicht - wie in anderen Programmiersprachen - mit `switch/case` umsetzen. Stattdessen müssen `if/elif/else`-Konstrukte erstellt werden:

Diejenigen, die noch das Kinderbuch "Das Sams" kennen, wissen, dass Herr Taschenbier an den Wochentagen immer auf bestimmte Ereignisse gewartet hat. Worauf er wartet lässt sich beispielsweise so ausdrücken:

![Struktogramm mit Mehrfachauswahl nach Wochentagen](images/bedingteAnweisung4.PNG)

In Python sind so Ausdrücke leider immer etwas unübersichtlich. Da hier jeweils nur eine Operation in jeder _Suite_ aufgerufen wird, könnte der Zeilenumbruch nach dem Doppelpunkt auch weggelassen werden. Darunter würde aber die Lesbarkeit noch weiter leiden.

```python
tag = input("Welcher Wochentag ist heute")
if  (tag == "Sonntag"):
  print("Die Sonne scheint!")
elif(tag   == "Montag"):
  print("Herr Mon kommt!")
elif (tag == "Dienstag"):
  print("Ich habe Dienst!")
elif (tag == "Mittwoch"):
  print("Es ist Mitte der Woche!")
elif (tag == "Donnerstag"):
  print("Es donnert!")
elif (tag == "Freitag"):
  print("Ich habe Frei!")
elif (tag == "Samstag"):
  print("Das Sams kommt!")
else:
  print("Das ist gar kein Tag...")
```

### Kurzschreibweise:

Python bietet die Möglichkeit, bei Wertzuweisungen eine Kurzform zu verwenden. Hierbei weicht die Reihenfolge der Operanden und Operatoren leicht von den obigen Beispielen ab: zuerst wird der Wert bei zutreffender Bedingung genannt.

`WERT_WENN_TRUE if BEDINGUNG else WERT_WENN_FALSE`

Als Beispiel wird hier die Zeichenkette "gerade" bei allen durch zwei restlos teilbaren Ganzzahlen zugewiesen, andernfalls "ungerade":

```python
n = 3
s = 'gerade' if n%2==0 else 'ungerade'
```

### Fazit

Drei wichtige _Take-Home-Messages_ gibt es als Besonderheit bei bedingten Anweisungen in Python:

* Codeblöcke werden per Einrückung als sogenannte _Suites_ gebildet.

* Weitere bedingte Verzweigungen werden (aus unerfindlichen Gründen) mit `elif` eingeleitet - es gibt also keine `switch/case`-Konstrukte.

* Es gibt eine Kurzschreibweise (`s = 'gerade' if n%2==0 else 'ungerade'`), die Platz spart aber gewöhnungsbedürftig ist. Möge jede/r selbst entscheiden, ob es zur Wartbarkeit des Codes beiträgt.

### Nächste Schritte

Den Kontrollfluss kann man neben den alternativen Verzweigungen auch mit Wiederholungsstrukturen steuern. Mehr dazu im folgenden Text zu [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen).



