## Python-Coding-Kata: ASCII-Art Weihnachtsbaum schmücken


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_coding_kata_weihnachtsbaum</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/113476982438066198</span>

> **tl/dr;** _(ca. 3 min Lesezeit - Bearbeitungszeit ca. 3WS): Fingerübung zu Kontrollstrukturen, Wiederholungsstrukturen und Operatoren in Python: Ein ASCII-Art Weihnachtsbaum soll auf der Konsole ausgegeben und geschmückt werden._



### Weihnachtsbaum als ASCII-Art

Es soll im folgenden in mehreren Iterationsschritten eine Funktion erstellt werden, die einen Weihnachtsbaum vorgegebener Höhe auf der Konsole ausgibt. Die Funktion soll die folgende Signatur haben:

```python
def weihnachtsbaum(hoehe: int)->str:
    pass
```

Implementiert die Methode weihnachtsbaum() mit folgenden Vorgaben:

#### Iteration 1:

Der Weihnachtsbaum soll zunächst als schiefe Pyramide mit Rauten (Hashtag, "#") gebaut werden. Die Funktion `weihnachtsbaum(hoehe: int)` soll zunächst direkt per `print()` den Weihnachtsbaum auf der Konsole ausgeben. Jede Ebene des Baums soll aus einem zusätzlichen Element bestehen:


```
#
##
###
####
#####
######
```

<span class="hidden-text">

```python
def weihnachtsbaum(hoehe: int)->str:
    
    for i in range(1,hoehe+1):
       print("#"*i) 

weihnachtsbaum(7)
```

</span>

#### Iteration 2:

Der Baum soll gerade aufgerichtet werden und einen zentralen Stamm haben (eine Raute unten in der Mitte, der nicht in die Höhe zählt). Es können zwei unterschiedliche Varianten gebaut werden:

Klein und dick:

```
    #    
   ###
  #####
 #######
    #
```

Groß und schief:

```
   #
   ##
  ###
  ####
 #####
 ######
#######
   #
```

Beschreibt am Besten zunächst, aus wie vielen Leerzeichen und aus wie vielen Rauten jede Zeile besteht, und versucht dann, den Baum zu implementieren.

<span class="hidden-text">
Weihnachtsbaum, Größe 7
   #    //3 LZ + 1*#
   ##   //3 LZ + 2*#
  ###   //2 LZ + 3*#
  ####  //2 LZ + 4*#
 #####  //1 LZ + 5*#
 ###### //1 LZ + 6*#
####### //0 LZ + 7*#
   #    //3 LZ + #
</span>

#### Iteration 3:

Der Weihnachtsbaum soll "Zacken" für die Zweige bekommen: die obersten nach 3 Reihen (jede Seite eins kürzer), dann nach weiteren 4 (jede Seite zwei kürzer), nach 5 (jede Seite drei Kürzer), 6... Bei einem Zacken soll es an jeder Seite eine Raute zurück gehen:

```
      #
     ###
    #####
     ###
    #####
   #######
  #########
    #####
   #######
  #########
 ###########
#############
      #
```

<span class="hidden-text">

</span>

#### Iteration 3:

Wer so weit ist, muss sich Gedanken um Schmücken mit Kugeln machen... Es sollen der Funktion ein Parameter übergeben werden, wie viele Kugeln zufällig verteilt erscheinen sollen ("O").

Ausserdem soll die Ausgabe nun nicht mehr in der Funktion `weihnachtsbaum()` erfolgen, sondern diese soll nur eine Zeichenkette erstellen, die dann per `return` zurückgegeben wird. Der Ausdruck auf der Konsole erfolgt direkt bei Funktionsaufruf: `print(weihnachtsbaum(12, 6))`.

```python
def weihnachtsbaum(hoehe: int, kugelzahl: int)->str:
    pass
```

```
      #
     ###
    #O###
     ###
    ##O##
   #O#####
  #########
    ###O#
   #######
  ##O####O#
 O####O#####
#############
      #
```

<span class="hidden-text">
           #
         #####
          ###
        #######
      ###########
       #########
     #############
   #################
    ###############
  ###################
#######################
           ##
</span>

#### Iteration 4:

Wenn der Baum geschmückt ist, müssen noch Kerzen verteilt werden (ebenfalls per Parameter). Kerzen werden über zwei Zeilen verteilt (Pipe als Kerze "|" und "d" als Flamme):


```python
def weihnachtsbaum(hoehe: int, kugelzahl: int, kerzenzahl: int)->str:
    pass
```

```
      #
     ###d
    #O##|
     ###
    ##O##
   #O###d#
  ##d###|##
    |##O#
   #######
  d#O####O#
 O|###O##### 
#############
      #
```

#### Iteration 5:

Na gut, es gibt noch viele Variationen: Baum in breite variieren, statt "#" beliebiges Zeichen zulassen, Baum ohne Füllung, Baum aus Schrägern: 

```
    /\
   /  \
   /  \
  /    \
  /    \
 /      \
/---||---\
    ||
```
... oder ....

```
    /\
   //\\
   //\\
  ///\\\
  ///\\\
 ////\\\\
/---||---\
    ||
```

oder 

```
           #
         #####
          ###
        #######
      ###########
       #########
     #############
   #################
    ###############
  ###################
#######################
           ##
```

oder ganz anders? Ich höre gerne per Fediverse (link unten) von neuen Baum-Ideen!

Kurzum: ab der fünften Iteration ist jede/r auf sich selbst gestellt.

Großer Nachteil: diese Art der Weihnachtsbastelei lässt sich leider kaum als Geschenk verschicken. Oder doch?
 