## Objektsammlungen in Listen (Python)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_listen</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Listen stellen eine mächtige und flexible Objektsammlung in Python dar. Der Artikel zeigt, wie Elemente erzeugt, verändert, gelöscht und ausgegeben werden._

### Grundlagen

Die Elemente von Listen können geändert, gelöscht und angefügt werden (sie sind mutable). Wie bei Tupeln kann auf die einzelnen Elemente über deren Index zugegriffen werden und es können Dopplungen enthalten sein. Listen sind in ihrer Länge variabel und verfügen über eine innere Ordnung.

Listen sind in Python als eine Klasse implementiert, die eine Reihe von Methoden anbietet - viele davon sind bereits durch den Namen selbsterklärend.

![Methoden der Klasse _list_](plantuml/list.png)

Am besten kann man Listen kennenlernen, in dem man sie mithilfe der Pythonshell erforscht:

### Schritt 1: Einfache Listenoperationen

Es wird eine einfache Liste erzeugt, die die Werte des Spiels "Schere, Stein, Papier" speichert und verwaltet.

Einer neuen Liste Werte zuweisen:

```python
>>> gewaehlt=["Stein", "Schere", "Stein", "Papier", "Schere"]
```

Den Datentyp eines List-Objekts ausgeben:

```python
>>> type(gewaehlt)
<class 'list'>
```

Den Inhalt einer List auf der Konsole ausgeben:

```python
>>> gewaehlt
['Stein', 'Schere', 'Stein', 'Papier', 'Schere']
```

Ein bestimmtes Element einer List ausgeben

```python
>>> gewaehlt[2]
'Stein'
```

Ebenso lassen sich über den Index `0` das erste und über den Index `-1` das letzte Element ansprechen.

Ein Element einer Liste (an bestehender Position) zuweisen

```python
>>> gewaehlt[2] = 'Holz'
```

#### Die Länge einer Liste ausgeben

```python
>>> len(gewaehlt)
5
```

Ein Element einer Liste löschen

```python
>>> del gewaehlt[2]
```

### Übungen zu einfachen Listenoperationen

Gegeben ist die folgende gefüllte Liste:

```python
haustierliste = ["Hund", "Katze", "Maus", "Hamster", "Meerschweinchen"] 
```
1. Schreibe ein Programm, dass die Liste erzeugt und auf der Konsole ausgibt

2. Ändere vor der Ausgabe Maus zu Wellensittich

3. Lösche vor der Ausgabe die Katze

4. Frage den Benutzer nach der Ausgabe nach dem Index des Tiers, das er austauschen will und nach dem Namen des Tiers, dass stattdessen dort stehen soll.

5. Gebe die neue Liste und die Länge der Liste aus.

### Schritt 2: Listenmethoden

#### Eine leere Liste erzeugen

```python
>>> spielkarten = []
```

#### Elemente an eine Liste anhängen

```python
>>> spielkarten.append("Pik")
>>> spielkarten.append("Karo")
```

Ein neues Element am Index 1 (2. Position) einfügen:

```python
>>> spielkarten.insert(1,"Herz")
```

Ein neues Element am Index 0 (1. Position) einfügen:

```python
>>> spielkarten.insert(0,"Kreuz")
```

Ein neues Element an die vorletzte Position einfügen:

```python
>>> spielkarten.insert(-1,"Schelln")
```

Ein neues Element an die letzte Position einfügen - macht man natürlich eigentlich mit `insert()`, aber so ginge es auch:
```python
>>> spielkarten.insert(len(spielkarten),"Grün")
```

#### Iterieren durch Listen

```python
for karte in spielkarten:
  print(karte)
```

### Übung zu Listenmethoden

1. Erstelle eine leere Liste, in der die Marvel-Charaktere gespeichert werden sollen. Nenne die Liste "Avengers"

2. Füge der Liste per Methode das neue Element "Iron Man" an

3. Füge der Liste per Methode die neuen Elemente "Captain America" und "Thor" an

4. Gebe die Liste aus.

5. Füge an der zweiten Stelle "Black Widow" ein.

6. Füge an der fünften Stelle "Superman" ein.

6. Füge an der sechsten Stelle "Bat man" ein.

7. Füge an der vierten Stelle "Hulk" ein.

8. Lösche das letzte Element.

9. Ersetze das fünfte Element durh "Hawkeye" 

10. Gebe die Liste aus.

11. Schon fertig? Dann erstelle eine Schleife, die so lange läuft, bis der Benutzer "e" (für Ende) eingibt, in der der Benutzer

  a) per "l" die aktuelle Liste ausgeben kann

  b) per "c" einen neuen Wert an eine gebene Position einfügen kann,
  
  c) per "r" den Wert an einer einzugebenden Position ausgeben kann, 

  b) per "u" einen Wert (an eingegebener Position) updaten kann,

  e) per "d" einen Wert (an eingegebener Position) löschen kann.


#### Listen als Referenztypen

Listen verhalten sich anders als Werttypen. Welches Verhalten verwundert, wenn man folgendes ausprobiert? Was lässt sich daraus für Listen ableiten?

```python
>>> haustiere = ["Hund", "Katze", "Maus"]
```

```python
>>> peters_haustiere = haustiere
>>> marthas_haustiere = haustiere
```

```python
>>> peters_haustiere
['Hund', 'Katze', 'Maus']
```

```python
>>> peters_haustiere[2]="Hamster"
```

```python
>>> marthas_haustiere
['Hund', 'Katze', 'Hamster']
```

### Teillisten / Slices

Wir können in Python relativ einfach Listenabschnitte kopieren, sogenannte _Slices_. Die Notation ist dabei

```python
teil_liste = komplette_liste[start:ende]
```


Wobei `start` der erste Index ist, der in der Teilliste enthalten sein soll und `ende` der erste Index ist, der nicht mehr in der Liste enthalten sein soll.

Die folgenden Beispiele werden mit der folgenden Liste umgesetzt, die Ursprungsliste selbst wird hierbei nicht verändert, sondern lediglich eine Kopie erzeugt:

```python
>>> marthas_haustiere = ['Hund', 'Katze', 'Hamster']
```

```python
>>> haustiere[1:2]
['Katze']
```

```python
>>> marthas_haustiere[0:2]
['Hund', 'Katze']
```

```python
>>> marthas_haustiere[:2]
['Hund', 'Katze']
```
```python
>>> marthas_haustiere[2:]
['Hamster']
```

```python
>>> marthas_haustiere[1:-1]
['Katze']
```

```python
>>> marthas_haustiere[-1:2]
[]
```

Slices können auch zur Datenverarbeitung genutzt werden.

### Type-Annotations für Listen

Type-Annotations geben Hinweise auf verwendete Datentypen, ohne eine funktionale Bedeutung für die Ausführung des Codes zu haben. Sie sind in Python optional und dienen ähnlich wie Kommentare nur dem Verständnis. Für Listen sind keine Type-Annotations im normalen Sprachumfang von Python enthalten. Sie müssen daher aus `typing` importiert werden.

Will man beschreiben, dass in `marthas_haustiere` eine Liste aus Zeichenketten gespeichert werden soll, so lautet der Code folgendermaßen:

```python

from typing import List

marthas_haustiere: List[str]

```


## Weitere Literatur zu Listen

- [Python-Dokumentation zu Listen](https://docs.python.org/3/library/stdtypes.html#sequence-types-list-tuple-range)

