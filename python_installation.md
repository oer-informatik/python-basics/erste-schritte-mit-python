## Installation von Python


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_installation</span>


> **tl/dr;** _(ca. 3 min Lesezeit): Eine kurze Beschreibung, worauf bei der Installation von Python geachtet werden muss und wie der Pfad von Python in den Umgebungsvariablen eingetragen werden kann._


Dieser Artikel ist Bestandteil einer Grundlagenserie zu Python:

- einfache Programmierung in Python: [Installation](https://oer-informatik.de/python_installation) / [Datentypen](https://oer-informatik.de/python_datentypen) / [Operatoren](https://oer-informatik.de/python_operatoren) / [if: alternative Kontrollstrukturen](https://oer-informatik.de/python_alternative_kontrollstrukturen) / [Schleifen (wiederholende Kontrollstrukturen)](https://oer-informatik.de/python_wiederholungsstrukturen) / [Funktionen](https://oer-informatik.de/python_funktionen) / [Objektsammlungen](https://oer-informatik.de/python_objektsammlungen) / [Dictionary](https://oer-informatik.de/python_dictionary) / [Listen](https://oer-informatik.de/python_listen)

- objektorientierte Programmierung in Python: [OOP Grundlagen: Klassen, Objekte](https://oer-informatik.de/python_oop) / [Kapselung von Attributen](https://oer-informatik.de/python_oop_kapselung) / [Vererbung](https://oer-informatik.de/uml-klassendiagramm-vererbung) / [Unit-Tests mit pytest](https://oer-informatik.de/python_einstieg_pytest)

### Python installieren

Auf der Website [python.org](https://www.python.org/) die zum OS passende Python-Datei wählen...

![Betriebssystemauswahl auf python.org](images/00_download_python.png)

... die heruntergeladene Datei als Administrator ausführen...

![Kontextmenü "Als Administrator ausführen"](images/pythonInstallieren01.png)

... den Python-Ordner in der Umgebungsvariable ´PATH´ anfügen (um ohne Pfadangabe immer darauf zugreifen zu können) und "Customize Install" auswählen, damit einige Anpassungen vorgenommen werden können...

!["Add Python to Path" und "Customize" wählen](images/pythonInstallieren02.png)

... sämtliche Optionen auswählen...

![Alle Optionen installieren](images/pythonInstallieren03.png)

... die "debugging symbols" und "debug binaries" sind zunächst nicht erforderlich (nur in Windows-Systemen).

![Alles bis auf die Debugging-Optionen wählen](images/pythonInstallieren04.png)

... Installation abgeschlossen.

!["Installation was successful"](images/pythonInstallieren05.png)

### Python Installation überprüfen

Wurde die Umgebungsvariable korrekt gesetzt? Das kann unter Windows in den "Systemeigenschaften" geprüft werden:
Windows-Taste, danach `sysdm.cpl` eingeben
Im Tabulator "Erweitert /Advanced" finden sich unten die Umgebungsvariablen:

![Systemeigenschaften-Fenster](images/addPythonToPath01.png)

In der unteren Tabelle sollte "Path" stehen. Diesen Eintrag auswählen und "Bearbeiten" anklicken...

!["Path" bearbeiten](images/addPythonToPath02.png)

Es sollte der Ordner erscheinen, in dem Python installiert wurde und der Unterordner `Scripts`. Falls dies nicht der Fall ist (weil in der Anleitung oberhalb vergessen wurde das Häkchen bei `Path` zu setzen) kann dieser Eintrag per "Neu" ergänzt werden (der jeweilige Pfad muss natürlich angepasst werden!).

![Pfad zu Python und Scripts ergänzen / anpassen](images/addPythonToPath03.png)

### Nächste Schritte

Als nächstes betrachten wir direkt in der Python-Shell die unterschiedlichen [Datentypen](https://oer-informatik.de/python_datentypen), die Python bietet.

## Links und weitere Informationen

- [python.org](https://www.python.org/)

