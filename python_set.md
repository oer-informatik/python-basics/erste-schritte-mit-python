## Objektsammlungen in Sets

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_set</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Mit Sets bietet Python einen Objektsammlungstyp, der dafür sorgt, dass jedes Element einzigartig ist. Die verfügbaren Methoden der set-Klasse werden in diesem Artikel an Beispielen vorgestellt._

### Grundlagen

Sets sind ungeordnete Sammlungen einzigartiger (unikaler) Elemente, d.h. jedes Objekt kann genau einmal vorkommen.

```python
>>> karten={"Herz", "Karo", "Kreuz", "Pik"}
>>> type(karten)
<class 'set'>
>>> karten
{'Pik', 'Kreuz', 'Herz', 'Karo'}
```
Sets sind dynamische, veränderbare (mutable) Objektsammlungen ohne garantierte Ordnung innerhalb der Elemente. Sie enthalten keine Duplikate (Dopplungen). Sie werden initialisiert und instanziiert, in dem die einzelnen Komma-getrennten Elemente in geschweiften Klammern eingefasst werden:

```python
>>> karten_farbe = {'Kreuz', 'Pik', 'Herz','Karo'}
>>> type(karten_farbe)
<class 'set'>
```

### Unterschied zu Listen

Der Unterschied zu einer `list` wird in folgendem Beispiel deutlich:

Zunächst erstellen wir eine `list` mit allen Spielkarten, die ein Spieler bei MauMau auf der Hand hat:

```python
>>> mau_mau_blatt=['KaroAss', 'KreuzBube', 'KaroAss', 'PikSieben', 'KreuzBube', 'KaroAss']
```

Sicherheitshalber prüfen wir nochmal, ob es sich wirklich um eine `list` handelt. Durch die eckigen Klammern bei der Instanziierung wurde dies festgelegt:

```python
>>> type(mau_mau_blatt)
<class 'list'>
```

Wir wandeln diese `list` in ein `set` um über die Python-Funktion `set()` und prüfen, ob der Datentyp stimmt:

```python
>>> unterschiedliche_karten=set(mau_mau_blatt)
>>> type(unterschiedliche_karten)
<class 'set'>
```

Es handelt sich um ein Set. Sets dürfen keine Duplikate enthalten - in der `list` waren jedoch einige Spielkarten doppelt. Wir überprüfen das: Enthält das `set` noch Dopplungen?

```python
>>> unterschiedliche_karten
{'KreuzBube', 'PikSieben', 'KaroAss'}
```

Offensichtlich wurden die Duplikate entfernt.

### Mengenlehre mit Sets...

Mithilfe von Sets lassen sich Mengenoperationen vornehmen. Wir wollen das an Hand eines Pizzabeispiels aufzeigen. Herta und Manfred haben sich Pizzabelage ausgesucht und in Sets gespeichert:

```python
hertas_pizza_zutaten={'Pepperoni', 'Käse', 'Zuccini', 'Zwiebeln', 'Oliven', 'Tomate'}
manfreds_pizza_zutaten={'Artischocken', 'Sardellen', 'Käse', 'Schafkäse', 'Oliven', 'Tomate'}
```

Welche Zutaten mögen Manfred und Herta? Die Schnittmenge beider Sets definiert, welche Elemente in beiden Sets vorkommen.

![Schnittmenge aus den Vorlieben von Herta und Manfred](images/mengenlehre.png)



Mit der Set-Methode `intersection()` kann ein Set mit allen Elementen ausgegeben werden, die in beiden Sets vorkommt:

```python
>>> manfreds_pizza_zutaten.intersection(hertas_pizza_zutaten)
{'Oliven', 'Käse', 'Tomate'}
```

Was mag Manfred, was Herta nicht mag? Die Methode `difference()` löscht aus der Ergebnismenge alle Elemente, die auch im  übergebenen Set vorkommen:

```python
>>> manfreds_pizza_zutaten.difference(hertas_pizza_zutaten)
{'Artischocken', 'Sardellen', 'Schafkäse'}
```

Was mag Herta, was Manfred nicht mag? Natürlich geht das auch andersherum:
```python
>>> hertas_pizza_zutaten.difference(manfreds_pizza_zutaten)
{'Zwiebeln', 'Pepperoni', 'Zuccini'}
```

Welche Elemente sind insgesamt vorhanden (also in mindestens einer der beiden Sets)? Hierüber gibt die Vereinigungsmenge Auskunft (Methode: `union()`). Was muss also eingekauft werden, wenn Manfred und Herta zusammen kochen wollen?

```python
>>> manfreds_pizza_zutaten.union(hertas_pizza_zutaten)
{'Käse', 'Zwiebeln', 'Oliven', 'Schafkäse', 'Artischocken', 'Sardellen', 'Tomate', 'Zuccini', 'Pepperoni'}
```
Bleibt die Frage, welche Elemente nur in einer der beiden Sets vorkommen (also etwa `union()` minus `intersection()`). Hier hilft uns `symmetric_difference()`:

```python
>>> manfreds_pizza_zutaten.symmetric_difference(hertas_pizza_zutaten)
{'Zwiebeln', 'Schafkäse', 'Artischocken', 'Sardellen', 'Zuccini', 'Pepperoni'}
```

### Übersicht zu den Methoden eines Set

Die wesentlichen Methoden, die die Python-Klasse `set` anbietet, ist in nachfolgendem UML-Klassendiagramm dargestellt:

![UML-Klassendiagramm für Sets](plantuml/set.png)


### Weitere Literatur zu Sets

- [Python-Dokumentation zu Sets](https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset)

