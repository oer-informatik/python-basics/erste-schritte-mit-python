## Python-Coding-Kata: Energieverbrauch für Mobilität


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_coding_kata_mobilitaet</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/</span>

<span class="hidden-text">

</span>

> **tl/dr;** _(ca. 20 min Lesezeit - Bearbeitungszeit ca. 8WS): Fingerübung zur Einführung von Kontrollstrukturen, Ein-/Ausgabe auf der Konsole und Funktionen in Python: Wie berechne und vergleiche ich den Energieaufwand für unterschiedliche Fahrzeuge bei unterschiedlicher Geschwindigkeit?_


### Grundidee

Die Fortbewegung mit Fahrzeugen benötigt Energie. Wir wandeln Energie, die in Mineralöl oder in Batterien chemisch gespeichert ist in Bewegungsenergie (kinetische Energie) um.

Die Motor verrichtet an der Welle (also der Achse bzw. an den drehenden Rädern) Arbeit (_work_). Diese Arbeit ($W_{welle}$) wird benötigt, um

* Rollreibung zu überwinden ($W_{rr}$),

* Luftreibung zu überwinden ($W_{cw}$),

* Beschleunigungsarbeit zu verrichten  ($W_{v}$) und

* die potenzielle Energie des Fahrzeugs zu ändern, wenn Start und Ziel auf unterschiedlichen Höhen liegen (($W_{g}$)).

$$ W_{welle} = W_g + W_v + W_{rr} + W_{cw}$$

![Aufteilung der Primärenergie auf Luftreibung, Rollreibung, pot. Energie und Beschleunigung](images/sankey.png)

In Abhängigkeit von dem verwendeten Antriebssystem wird zudem ein großer Teil der Energie bereits im Motor in Wärme umgewandelt und steht an der Motorwelle überhaupt nicht zur Verfügung.

Es soll in mehreren Iterationen ein einfaches Programm erstellt werden, das aus den einzelnen Komponenten die Beträge errechnet - abhängig vom Fahrzeugtyp.

Das Programm soll am Ende nach Eingabe des Fahrzeugtyps (z.B: Sportcoupé, SUV, Van, Kleinwagen, Rennrad, Hollandrad, eBike ) und ggf. zusätzlich nötiger Informationen (siehe unten) die benötigte Energie für eine Fahrtstrecke von 100 km ausgeben.


### Iteration 1: Benötigte Arbeit zur Überwindung des Rollwiderstands

Im ersten Schritt betrachten wir nur die Arbeit zur Überwindung der Rollreibung ($W_{rr}$). Diese berechnet sich aus:

$$W_{rr} = \mu_{rr} \cdot m  \cdot g  \cdot s$$

Die einzelnen Formelbestandteile bedeuten:

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$\mu_{rr}$|Rollreibungskoeffizient PKW auf Asphalt|$0{,}0085$|
|$m$|Masse des Fahrzeugs (exkl. Insassen)|$929\ \mathrm{kg}$ (z.B: VW Up)|
|$g$|Erdbeschleunigung|$9{,}81 \mathrm{\frac{m}{s^2}}$|
|$s$|zurückgelegte Strecke in Metern|$100.000\ \mathrm{m}$|



Das Gewicht der Fahrer*innen und Insassen muss zusätzlich abgefragt werden.

Es soll ein Programm erstellt werden, das

* die zurückgelegte Strecke in Kilometern abfragt,

* daraus die Rollreibungsarbeit berechnet und

* auf der Konsole ausgibt,

* auf die zugehörigen Einheiten achtet.

Wenn die Strecke in Metern angegeben wird errechnet sich der Wert zu Wattsekunden:


$$ Einheit(W_{rr}) = kg \cdot \frac{m}{s^2}  \cdot m =  \frac{kg \cdot m^2}{s^2}=  Ws $$

Die Umwandlung einer Wattsekunde in Kilowattstunden erfolgt durch den Faktor:

$$\frac{1 \mathrm{kWh}}{1000 \mathrm{Wh}} \cdot \frac{1 \mathrm{h}}{3.600\mathrm{s}}  = \frac{1\  kWh}{3.600.000\ Ws}$$

(Also einfach das Ergebnis, dass sie mit eingegebener Strecke in Metern erhalten durch 3.600.000 teilen.)

Zu schnell fertig? Dann kann das Programm in erster Iteration erweitert werden:

* Zusätzlich soll noch das Gewicht der Insassen erfasst und be der Berechnung berücksichtigt werden.

* Es soll überprüft werden, ob es sich bei der Eingabe um gültige Zahlen handelt. Nur bei Zahlen soll die Berechnung weitergeführt werden.

* Die Abfrage soll so lange erfolgen, bis als Eingabe an Stelle einer Zahl das Wort "exit" oder "Exit" erfolgt.


### Iteration 2: Benötigte Arbeit zur Überwindung des Luftwiderstands

Jetzt folgt der spannende Teil:

![Benzinverbrauch in Abhängigkeit von der Geschwindigkeit](images/benzinverbrauch-geschwindigkeit.png)

Wenn wir schneller Auto fahren (oder schneller Fahrrad fahren) benötigt das mehr Energie. Neben der Rollreibung $W_{rr}$ ist also noch eine weitere Komponente relevant, die wir hinzuaddieren müssen: Die Luftreibung.

$$W_{cw} = 0.5 \cdot c_w  \cdot A  \cdot \rho_{luft}  \cdot v^2   \cdot s $$

Die einzelnen Werte für die Faktoren oben ergeben sich zu:

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$c_w$|Strömungswiderstandskoeffizient^[Liste z.B. [hier](https://automobil-guru.de/cw-werte-tabelle-stirnflaeche/)]|$0.32$: Kleinwagen (VW Up)|
|$A$|Stirnfläche^[Liste z.B. [hier](https://automobil-guru.de/cw-werte-tabelle-stirnflaeche/)]|$2.07\ m^2$: Kleinwagen (VW Up)|
|$\rho_{luft}$|Dichte der Luft|$1.2\ \frac{kg}{m^3}$ (bei 20° C)|
|$s$|Gefahrene Kilometer| z.B. $100\ km$|
|$v$|Geschwindigkeit|Wenn die Eingabe in $\frac{km}{h}$ erfolgt, muss noch mit dem Faktor $\frac{1000\ \frac{m}{km}}{3600\ \frac{s}{h}}$ multipliziert werden|

Das obige Programm soll so weitert werden, dass es

* die Geschwindigkeit in km/h abfragt,

* daraus die Arbeit zur Überwindung des Luftwiderstands berechnet und

* diesen gemeinsam mit der Rollreibung ausgibt.

Hierzu sollen jetzt zwei Funktionen genutzt werden: die den Betrag der Arbeit zurückgeben:

```python
def berechne_rollreibungsarbeit(reibungskoeff: float, masse: float, strecke: float) -> float:
    """Berechne den Arbeitsanteil der Rollreibung in Kilowattstunden (kWh)"""
    return 0
```

```python
def berechne_luftreibungsarbeit(cw_wert: float, stirnflaeche: float, strecke: float, geschwindigkeit: float) -> float:
    """Berechne den Arbeitsanteil der Luftreibung in Kilowattstunden (kWh)"""
    return 0
```

Wenn die Geschwindigkeit in $\frac{km}{h}$ angegeben wird müssen wir das Ergebnis noch umwandeln, um es wieder in Kilowattstunden zu erhalten:

$$W_{cw}= \cdot c_w \cdot A \cdot \rho_{luft} \cdot v^2 \cdot s $$


$$ Einheit(W_{cw}) =  m^2 \cdot \frac{kg}{m^3} \cdot \frac{km^2}{h^2} \cdot m  = \frac{kg \cdot km^2}{h^2} $$

$$ 1 \cdot \frac{kg \cdot km^2}{h^2} = 1 \cdot  \frac{kg \cdot 1.000.000 \ m^2}{12.960.000 \ s^2} = \frac{1}{12,96} Ws $$

$$   = \frac{1}{12,96} \cdot \frac{1}{3.600.000} kWh $$

Wir müssen das Ergebnis also durch 3.600.000 und durch 12,96 teilen um einen Wert in Kilowattstunden zu erhalten.

Erweiterung:

* Fehleingaben sollen abgefangen werden, in dem alle Werte, die keine Zahl sind zu "0" werden.

* Fehleingaben sollen abgefangen werden, in dem fehlerhaft eingegebene Kommata in Dezimalpunkte umgewandelt werden.

* Die Ausgabe soll immer für unterschiedliche Geschwindigkeiten erfolgen: $30 \frac{km}{h}$,  $50 \frac{km}{h}$,  $80 \frac{km}{h}$,  $100 \frac{km}{h}$,  $130 \frac{km}{h}$,  $160 \frac{km}{h}$,  $180 \frac{km}{h}$


![Benzinverbrauch je Fahrzeug bei 130 km/h](images/benzinverbrauch-fahrzeug.png)


### Iteration 3: Erweiterung der Eingabewerte durch Dictionaries

Es sollen unterschiedliche Fahrzeugtypen verglichen werden. Dazu sollen die Werte für Rollreibung, Masse, cw-Wert und Stirnfläche in einem Dictionary gespeichert werden. Mehrere dieser Dictionarys sollen in einer Liste der Fahrzeuge organisiert werden und die Berechnung der Vergleichswerte menügeführt erfolgen. Eine geeignete Menüstruktur soll selbständig geplant werden.

#### Detailwerte für die Rollreibung:

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$\mu_{rr}$|Rollreibungskoeffizient|PKW-Reifen (Kategorie C1) auf Asphalt, je Energieeffizienzklasse^[Werte aus der [EU-Verordnung](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:02009R1222-20120530&from=EN)]]<br/> $0,0085$ Effizienzklasse C+D (wählen wir exemplarisch)<br/>$0.003$ Rennrad<br/> $0.007$ Hollandrad/eBike^[Quelle: https://klara-agil.de/die-fahrwiderstaende-in-formeln.html]<br/><br/>Wer es genauer wissen will bei PKW:<br/>$0,0065$ Effizienzklasse A<br/>$0,007$ Effizienzklasse B<br/>$0,0096$ Effizienzklasse E<br/>$0,0113$ Effizienzklasse F|
|$m$|Masse des Fahrzeugs (exkl. Insassen)|$1465\ kg$: Sportcoupé (A5 F5 Coupé)<br/>$2125\ kg$: SUV (BMX X5 E70)<br/>$2300\ kg$: Van (VW Multivan T5)<br/>$929\ kg$: Kleinwagen (VW Up)<br/>$9\ kg$: Rennrad <br/>$17\ kg$: Hollandrad<br/>$25\ kg$: eBike|
|$g$|Erdbeschleunigung|$9,81 [\frac{m}{s^2}]$|
|$s$|zurückgelegte Strecke in Metern, z.B. |$100.000\ m$|

#### Detailwerte für die Luftreibung

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$c_w$|Strömungswiderstandskoeffizient^[Liste z.B. [hier](https://automobil-guru.de/cw-werte-tabelle-stirnflaeche/)]|$0.25$: Sportcoupé (A5 F5 Coupé)<br/>$0.37$: SUV (BMX X5 E70)<br/>$0.35$: Van (VW Multivan T5)<br/>$0.32$: Kleinwagen (VW Up)<br/>$0.4$: Rennrad<br/>$0.7$: Hollandrad / eBike|
|$A$|Stirnfläche^[Liste z.B. [hier](https://automobil-guru.de/cw-werte-tabelle-stirnflaeche/)]|$2.17\ m^2$: Sportcoupé (A5 F5 Coupé)<br/>$2.87\ m^2$: SUV  (BMX X5 E70)<br/>$3.25\ m^2$: Van (VW Multivan T5)<br/>$2.07\ m^2$: Kleinwagen (VW Up)<br/>$0.33\ m^2$ Rennradfahrer*in<br/>$0.53\ m^2$ Hollandrad/  eBike-Fahrer*in|
|$\rho_{luft}$|Dichte der Luft|$1.2\ \frac{kg}{m^3}$ (bei 20° C)|
|$s$|Gefahrene Kilometer| z.B. $100\ km$|
|$v$|Geschwindigkeit|Wenn die Eingabe in $\frac{km}{h}$ erfolgt, muss noch mit dem Faktor $\frac{1000\ \frac{m}{km}}{3600\ \frac{s}{h}}$ multipliziert werden|

Relevante Werte für die Fahrzeugtypen:

- für Autos den Verbrauch bei $30 \frac{km}{h}$,  $30 \frac{km}{h}$,  $50 \frac{km}{h}$,  $80 \frac{km}{h}$,  $100 \frac{km}{h}$,  $130 \frac{km}{h}$,  $160 \frac{km}{h}$,  $180 \frac{km}{h}$

- für Fahrräder bei  $15 \frac{km}{h}$,  $25 \frac{km}{h}$,  $45 \frac{km}{h}$

### Iteration 4: Benötigte Arbeit zur Überwindung eines Höhenunterschieds

Wenn wir von der Nordsee über den Brenner fahren wandeln wir Bewegungsenergie in potenzielle Energie um. Umgekehrt können wir bergab diese Energie nutzen - jedoch nur zum Teil. Da es bei Verbrennern bislang keine Rekuperation (Rückgewinnung) der Energie gibt, gehen wir davon aus, dass bergab $\frac{1}{4}$ der potenziellen Energie wieder genutzt werden kann. Bei E-Autos nehmen wir den Anteil mit $\frac{1}{2}$ an (hierfür habe ich noch keine Quellen, das sind also bislang  rein erfundene Zahlen!).


$$ W_g = m  \cdot g  \cdot h$$

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$m$|Masse des Autos|(siehe oben)|
|$g$|Erdbeschleunigung|(siehe oben)|
|$h$|Absolute Höhendifferenz über NN||

Das Programm soll um alle nötigen Abfragen und Ausgaben erweitert werden!

### Iteration 5: Berücksichtigung der Beschleunigungsvorgänge

Da bislang keine Rekuperation eingebaut ist werden Bremsvorgänge (Arbeitsbeträge mit negativem Vorzeichen) nicht berücksichtigt. Ein kompletter Beschleunigungsvorgang von $0 \frac{km}{h}$ bis $v$ benötigt:

$$ W_v = \frac{1}{2} \cdot m  \cdot v^2  \cdot n$$

mit

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$n$|Anzahl der Beschleunigungsvorgänge||
|$m$|Masse des Autos|(siehe oben))|
|$v$|Geschwindigkeit|(siehe oben))|

Wir nehmen für Autobahnfahrten vereinfachend an, dass wir 10 vollständige Beschleunigungsvorgänge pro 100 km benötigen. Diese Werte können mit Hilfe eines Smartphones oder einer Fitnessuhr gerne empirisch präzisiert werden!


### Iteration 6: Berücksichtigung der Antriebsart

Bislang haben wir nur die Arbeit an der Welle betrachtet und gänzlich die Verluste der unterschiedlichen Antriebsarten außer Betracht belassen. Der Primärenergieeinsatz hängt jedoch starkt vom Motortyp ab:

$$ W_{welle} = W_g + W_v + W_{rr} + W_{cw}$$

$$ W_{primär} = \frac{\eta}{W_g}$$

|Größe|Beschreibung|Beispielwerte|
|---|---|---|
|$\eta_{b}$|Wirkungsgrad eines Benzin-Verbrenners|$ca.\ 20\ \%$|
|$\eta_{d}$|Wirkungsgrad eines Diesel-Verbrenners|$ca.\ 33-40\ \%$|
|$\eta_{e}$|Wirkungsgrad eines e-Autos (inkl. Ladeverluste)|$ca.\ 64\ \%$|

### Vorschläge für weitere Iterationen:

- Iteration 7: Umrechnung in $CO_2$-Äquivalente

- Iteration 8: Umrechnung in Kosten

- Iteration 9: Grafische Ausgabe der berechneten Werte

...

### Links und weitere Informationen

- [Inspiriert durch den Spritrechner des VCD Herrenberg](https://vcd-herrenberg.de/xspritverbrauch/)

- Hintergrundinfos z.B. hier: [http://www.mx-electronic.com/pdf-texte/link-e-mobility/Der-Elektrofachmann-Wirkungsgrad-Vergleich-zwischen-Fahrz.pdf](http://www.mx-electronic.com/pdf-texte/link-e-mobility/Der-Elektrofachmann-Wirkungsgrad-Vergleich-zwischen-Fahrz.pdf)

